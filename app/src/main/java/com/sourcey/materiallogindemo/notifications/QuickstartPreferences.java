package com.sourcey.materiallogindemo.notifications;

/**
 * Created by lokal on 9/24/2017.
 */

public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}