package com.sourcey.materiallogindemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.activity.RedeemActivity;
import com.sourcey.materiallogindemo.model.Crime;
import com.sourcey.materiallogindemo.model.CrimeChild;

import java.util.List;

/**
 * Created by lokal on 8/19/2017.
 */

public class Tab2Adapter extends ExpandableRecyclerAdapter< Tab2Adapter.CrimeParentViewHolder, Tab2Adapter.CrimeChildViewHolder> {

    private LayoutInflater mInflater;

    /**
     * Public primary constructor.
     *
     * @param parentItemList the list of parent items to be displayed in the RecyclerView
     */
    public Tab2Adapter(Context context, @NonNull List<ParentObject> parentItemList) {
        super(context, parentItemList);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public CrimeParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {

        View view = mInflater.inflate(R.layout.item_tab2_parent, viewGroup, false);

        Button redeem = (Button) view.findViewById(R.id.redeem);
        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, RedeemActivity.class);
                mContext.startActivity(i);
            }
        });

        return new CrimeParentViewHolder(view);
    }

    @Override
    public CrimeChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.item_tab2_child, viewGroup, false);
        return new CrimeChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(CrimeParentViewHolder crimeParentViewHolder, int i, Object parentObject) {
        Crime crime = (Crime) parentObject;
        crimeParentViewHolder.mCrimeTitleTextView.setText(crime.getTitle());
    }

    @Override
    public void onBindChildViewHolder(CrimeChildViewHolder crimeChildViewHolder, int i, Object childObject) {
        CrimeChild crimeChild = (CrimeChild) childObject;
        crimeChildViewHolder.mCrimeDateText.setText(crimeChild.getYear());
        crimeChildViewHolder.mCode.setText("WQ657sdjnskd");
    }

    public class CrimeParentViewHolder extends ParentViewHolder {

        public TextView mCrimeTitleTextView;
        public Button mParentDropDownArrow;

        public CrimeParentViewHolder(View itemView) {
            super(itemView);

            mCrimeTitleTextView = (TextView) itemView.findViewById(R.id.parent_list_item_crime_title_text_view);
            mParentDropDownArrow = (Button) itemView.findViewById(R.id.details);
        }
    }

    public class CrimeChildViewHolder extends ChildViewHolder {

        public TextView mCrimeDateText;
        public TextView mCode;

        public CrimeChildViewHolder(View itemView) {
            super(itemView);

            mCrimeDateText = (TextView) itemView.findViewById(R.id.child_list_item_crime_date_text_view);
            mCode = (TextView) itemView.findViewById(R.id.text_code);
        }
    }
}
