package com.sourcey.materiallogindemo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.rewards.entities.Rewards;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 10/26/2017.
 */

public class DailyRewardAdapter extends ArrayAdapter<Rewards> {

    private ArrayList<Rewards> arrList;
    private Rewards currentReward;
    Context context;

    public DailyRewardAdapter(Context context, int textViewResourceId, ArrayList<Rewards> arrList, Rewards currentReward) {
        super(context, textViewResourceId, arrList);
        this.arrList = arrList;
        this.context = context;
        this.currentReward = currentReward;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.list_view_daily_rewards_item, null);
        }

        Rewards rewards = arrList.get(position);

        if (rewards != null){
            TextView txtDay = (TextView)view.findViewById(R.id.textDay);
            ImageView imgType = (ImageView)view.findViewById(R.id.imageType);
            TextView txtValue = (TextView)view.findViewById(R.id.textValue);

            txtDay.setText("Day " + (arrList.size() - position));
            txtValue.setText(String.valueOf(rewards.value));

            switch(rewards.type){
                case "coins": imgType.setImageResource(R.drawable.coins);
                              break;
                case "cards": imgType.setImageResource(R.drawable.cards);
                              txtValue.setText(txtValue.getText() + " extra");
                              break;
            }

            if (rewards.id.equals(currentReward.id)){
                view.setBackgroundColor(Color.rgb(115,198,204));
            }
        }
        return view;
    }
}
