package com.sourcey.materiallogindemo.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.sourcey.materiallogindemo.api.login.RefreshLogin;
import com.sourcey.materiallogindemo.api.login.entities.LoginData;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by K-Android 001 on 11/26/2017.
 */

public class SendRefreshTokenService extends Service {
    SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        preferences = getBaseContext().getSharedPreferences("user_info", MODE_PRIVATE);
        String refresh_token = preferences.getString("refresh_token", "");
        RefreshLogin refreshLogin = new RefreshLogin();
        refreshLogin.refreshLoginService().sendRefreshAuth(refresh_token).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.body()!=null){
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("access_token",response.body().getAccessToken());
                    editor.putString("refresh_token",response.body().getRefreshToken());
                    editor.commit();
                    Log.d("RefreshTokenResponse", "RefreshTokenSent");
                }
                else{
                    try {
                        Log.d("RefreshTokenResponse", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {

            }
        });
        return START_STICKY;
    }
}
