package com.sourcey.materiallogindemo.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;

import com.sourcey.materiallogindemo.R;

/**
 * Created by K-Android 001 on 11/9/2017.
 */

public class SoundService extends Service {
    private static final String TAG = null;
    public static MediaPlayer music;
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        music = MediaPlayer.create(this, R.raw.test);
        music.setLooping(true); // Set looping
        final SharedPreferences prefs = this.getSharedPreferences("user_info", MODE_PRIVATE);

//        if(prefs.getBoolean("isMusicOn", true))
//            music.setVolume(100,100);
//        else
            music.setVolume(0,0);

    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        music.start();
        return START_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TODO
    }

    public IBinder onUnBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public void onStop() {
        music.stop();
        music.release();
    }

    public void onPause() {

    }

    @Override
    public void onDestroy() {

        music.stop();
        music.release();
    }

    @Override
    public void onLowMemory() {

    }
}