package com.sourcey.materiallogindemo.layout;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;

import com.sourcey.materiallogindemo.R;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by lokal on 8/24/2017.
 */

public class Gameplay_layout extends View {

    private Bitmap pin;
    private float pin_x, pin_y;
    private float x,y;
    private boolean jump = false;
    private Context ctx;
    private float heightProportion, widthProportion;
    private ScrollView scroll;

    public Gameplay_layout(Context context) {
        super(context);
        /*BitmapDrawable bg = new BitmapDrawable(BitmapFactory.decodeResource(context.getResources(), R.drawable.path));
        bg.setGravity(Gravity.FILL_HORIZONTAL);
        bg.setTileModeY(Shader.TileMode.REPEAT);*/
        //setBackgroundDrawable(bg);

        //pin_x = 830;
        //pin_y = 1180;
        pin_x = 1; //* widthProportion;
        pin_y = 5;// * heightProportion;
        x = 0;
        y = 1;

        ctx = context;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("jump", false);
        editor.apply();

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        opts.outHeight = 7000;
        pin = BitmapFactory.decodeResource(getResources(), R.drawable.pin_img,opts);
        setProportions(width, height,pin );


    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        BitmapDrawable bg = new BitmapDrawable(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.path));
        bg.setTileModeY(Shader.TileMode.REPEAT);

        canvas.drawBitmap(bg.getBitmap(), 3000, 3000, null);
        canvas.drawBitmap(pin, convertDpToPixel(pin_x, ctx)* widthProportion, convertDpToPixel(pin_y, ctx)* heightProportion, null);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        Boolean jump = preferences.getBoolean("jump", false);

        if(jump){
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    pin_x = pin_x + x;
                    pin_y = pin_y + y;
                }
            }, 1000);

            canvas.drawBitmap(pin, convertDpToPixel(pin_x, ctx), convertDpToPixel(pin_y, ctx), null);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("jump", false);
            editor.apply();
        }

        invalidate();

    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    //Set the proportions for scaling in multiple devices
    public void setProportions(float screenWidth,float screenHeight,Bitmap background){

        this.heightProportion = screenHeight/background.getHeight();
        this.widthProportion = screenWidth/background.getWidth();

    }

    public Bitmap scaleBitmaps(Bitmap bitmap) {
        Bitmap out = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * widthProportion),
                (int)  (bitmap.getHeight()*heightProportion), false);
        return out;
    }
}
