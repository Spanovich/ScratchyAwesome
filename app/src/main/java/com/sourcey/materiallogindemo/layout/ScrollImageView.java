package com.sourcey.materiallogindemo.layout;

/**
 * Created by lokal on 8/29/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.OverScroller;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.activity.GameplayActivity;
import com.sourcey.materiallogindemo.model.Pixel;
import com.sourcey.materiallogindemo.model.ResponseHandler;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class ScrollImageView extends View implements GestureDetector.OnGestureListener {
    private final int DEFAULT_PADDING = 10;
    private Display mDisplay;
    private Bitmap mImage;
    private Bitmap pin, redRect, blueRect;
    private Boolean jump, discard;
    SharedPreferences preferences;

    private Matrix translate;
    //private Bitmap droid;
    private Matrix animateStart;
    private Interpolator animateInterpolator;
    private long startTime;
    private long endTime;
    private float totalAnimDx;
    private float totalAnimDy;

    private boolean isScrolling = false;

    private static final int WIDTH = 360;
    private static final int HEIGHT = 5760;
    private float oldY;
    /* Current x and y of the touch */
    private float mCurrentX = 0;
    private float mCurrentY = 0;

    private float clickableStepX = 110;
    private float clickableStepY = 280;

    /* To detect a click in onTouch listener */
    private float mStartX = 0;
    private float mStartY = 0;
    private float mEndX = 0;
    private float mEndY = 0;

    private float mTotalX = 0;
    private float mTotalY = 0;

    /* The touch distance change from the current touch */
    private float mDeltaX = 0;
    private float mDeltaY = 0;

    /* Current x and y of the touch */
    private float mPinCurrentX = 0;
    private float mPinCurrentY = 0;

    /*private float mPinTotalX = 185;
    private float mPinTotalY = -50;*/

    private float mPinTotalX = 240;
    private float mPinTotalY = 375;

    private float mPinTotalX2 = 162;
    private float mPinTotalY2 = 365;

    /* The touch distance change from the current touch */
    private float mPinDeltaX = 0;
    private float mPinDeltaY = 0;

    private int pinPosition = 1;

    private int currentPosition, lockPosition;

    int mDisplayWidth;
    int mDisplayHeight;
    int mPadding;

    private GestureDetector gestureDectector;
    private final Touch touch;
    private GameplayActivity ctx;

    ArrayList<Pixel> points;
    private Bitmap lockedRect;
    private String mTextField;
    private Bitmap unrevealed;
    private int i;
    private int titleBarHeight;

    private int w;
    private int h;
    private int scaledHeight;

    private int rocketPosition, giftboxPosition;

    public ScrollImageView(Context context, int titleBarHeight) {
        super(context);
        this.titleBarHeight = titleBarHeight;
        gestureDectector = new GestureDetector(context, this);
        touch = new Touch(context);
        touch.start();
        initScrollImageView(context);
        ctx = (GameplayActivity) context;
    }

    public ScrollImageView(Context context, AttributeSet attributeSet) {
        super(context);
        gestureDectector = new GestureDetector(context, this);
        touch = new Touch(context);
        touch.start();
        initScrollImageView(context);
    }

    private void initScrollImageView(Context context) {
        oldY = 0;
        currentPosition = 0;
        lockPosition = 9;
        //array of positions
        String json = parseJson();
        Gson gson = new Gson();
        ResponseHandler responseHolder = gson.fromJson(json, ResponseHandler.class);
        points = responseHolder.getPoints();

        if (currentPosition + 30 > points.size()) {
            expandPoints();
        }

        new CountDownTimer(900000, 1000) {

            public void onTick(long millisUntilFinished) {
                Long sec = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                mTextField = "" + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + ":" +
                        sec;
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                //mTextField = "done!";
            }

        }.start();

        //scale so the canvas is same on all devices
        mDisplay = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        mDisplayHeight = mDisplay.getHeight();
        mPadding = DEFAULT_PADDING;
        Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.sprite_final, null);
        Log.e("Height and width ", "height: " + ((Activity) context).getWindowManager().getDefaultDisplay().getHeight() + "Width : " + ((Activity) context).getWindowManager().getDefaultDisplay().getWidth());

        scaledHeight = HEIGHT * ((Activity) context).getWindowManager().getDefaultDisplay().getWidth() / WIDTH;

        setImage(background.createScaledBitmap(background, ((Activity) context).getWindowManager().getDefaultDisplay().getWidth(), scaledHeight, true));
// Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
// Convert the dps to pixels, based on density scale

       /* mPinTotalX = mPinTotalX * scale + 0.5f;
        mPinTotalY = mPinTotalY * scale + 0.5f;*/

        clickableStepX = clickableStepX * ((Activity) context).getWindowManager().getDefaultDisplay().getWidth() / WIDTH;
        clickableStepY = clickableStepY * scaledHeight / HEIGHT;
        for (int i = 0; i < points.size(); i++) {
            points.get(i).setX(points.get(i).getX() * ((Activity) context).getWindowManager().getDefaultDisplay().getWidth() / WIDTH);
            points.get(i).setY(points.get(i).getY() * scaledHeight / HEIGHT);
        }
        Log.d("clickable scale X: ", "clickable scale X: " + clickableStepX);
        Log.d("clickable scale X: ", "clickable scale Y: " + clickableStepY);

        //get drawable bitmaps
        pin = BitmapFactory.decodeResource(getResources(), R.drawable.pin, null);
        redRect = BitmapFactory.decodeResource(getResources(), R.drawable.giftboxfield, null);
        blueRect = BitmapFactory.decodeResource(getResources(), R.drawable.rocketlevelfield, null);
        lockedRect = BitmapFactory.decodeResource(getResources(), R.drawable.rsz_lock2, null);
        unrevealed = BitmapFactory.decodeResource(getResources(), R.drawable.emptyfield, null);

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("jump", false);
        editor.apply();


        i = 0;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = measureDim(widthMeasureSpec, mDisplay.getWidth());
        int height = measureDim(heightMeasureSpec, mDisplay.getHeight());
        setMeasuredDimension(width, height);
    }

    private int measureDim(int measureSpec, int size) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = size;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap image) {
        mImage = image;
    }

    public int getPadding() {
        return mPadding;
    }

    public void setPadding(int padding) {
        this.mPadding = padding;
    }


    @Override
    public boolean onTouchEvent(MotionEvent me) {
        boolean consumed = gestureDectector.onTouchEvent(me);
        if (consumed)
            return true;
        switch (me.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                return touch.down(me);
            case MotionEvent.ACTION_MOVE:
                return touch.move(me);
            case MotionEvent.ACTION_UP:
                return touch.up(me);
            case MotionEvent.ACTION_CANCEL:
                return touch.cancel(me);
        }
        return super.onTouchEvent(me);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if (mImage == null) {
            return;
        }

       /* try{
            giftboxPosition = Integer.parseInt(preferences.getString("giftbox", " "));
            rocketPosition = Integer.parseInt(preferences.getString("rocket", " "));
            giftPos = preferences.getString("giftbox", " ");
        }catch (Throwable t){
            t.getMessage().toString();
        }*/

        Log.d("Bojan", "onDraw");

        jump = preferences.getBoolean("jump", false);
        discard = preferences.getBoolean("discard", false);
        giftboxPosition = preferences.getInt("giftbox", 0);
        rocketPosition = preferences.getInt("rocket", 0);
        //jump is used for pin moving
        if (jump && currentPosition < lockPosition) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPosition++;
                    if (!discard) {
                        if (currentPosition == giftboxPosition) {
                            ctx.ShowGiftboxDialog();
                        } else {
                            if (currentPosition == rocketPosition) {
                                ctx.ShowLockedLevelDialog();
                            } else ctx.ShowScratchableDialog();
                        }
                    }

                }
            }, 1000);

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("jump", false);
            editor.apply();
        } else {


            if (i == 0) {
                mTotalY = mTotalY - points.get(currentPosition + 6).getY() + unrevealed.getHeight();
                i++;
            }

            float newTotalX = mTotalX + mDeltaX;
            // Don't scroll off the left or right edges of the bitmap.
            if (mPadding > newTotalX && newTotalX > getMeasuredWidth() - mImage.getWidth() - mPadding)
                mTotalX += mDeltaX;

            float newTotalY = mTotalY + mDeltaY;
            // Don't scroll off the top or bottom edges of the bitmap.
            //  Log.e("measured height : ", "measured heighht" + getMeasuredHeight());
            //  Log.e("image height : " , " image heirht" + mImage.getHeight());
            // Log.e("padding", "padding " + mPadding);
            //if (mPadding > newTotalY && newTotalY > getMeasuredHeight() - mImage.getHeight() - mPadding) {
            //  if (mPadding > newTotalY && newTotalY > getMeasuredHeight() - Math.abs(10000.00) - mPadding) {
            int top = (int) (newTotalY + points.get(currentPosition + 6).getY());
            Log.d("Bojan", "onDraw" + top);

            float maxYScrollOffset = 1500;
            if (currentPosition > 0 && currentPosition < points.size()) {
                Pixel currentPixel = points.get(currentPosition);
                maxYScrollOffset = currentPixel.getY();
            }

            boolean isSrdjanLazy = newTotalY + points.get(currentPosition + 6).getY() - unrevealed.getHeight() < 1500;
            float srdjanLazyLevel = newTotalY + points.get(currentPosition + 6).getY() - unrevealed.getHeight();

            if (currentPosition < 7) {
                if (isSrdjanLazy && srdjanLazyLevel > 0) {
                    if (isScrolling) {
                        mTotalY += mDeltaY;
                        //mPinTotalY += mPinDeltaY;
                        isScrolling = false;
                    }
                } else if (currentPosition > 7 && currentPosition < 12) {
                    if (isSrdjanLazy && srdjanLazyLevel > -500) {
                        if (isScrolling) {
                            mTotalY += mDeltaY;
                            //mPinTotalY += mPinDeltaY;
                            isScrolling = false;
                        }
                    }
                } else if (currentPosition > 12) {
                    if (isSrdjanLazy && srdjanLazyLevel > -1500) {
                        if (isScrolling) {
                            mTotalY += mDeltaY;
                            //mPinTotalY += mPinDeltaY;
                            isScrolling = false;
                        }
                    }
                }


            }

            Paint paint = new Paint();
            Log.d("Bojan", "totalY " + mTotalY);

            // Sprite/Image starting from top
            if (mTotalY < 0) {
                canvas.drawBitmap(mImage, 0, mTotalY % mImage.getHeight(), paint);
                canvas.drawBitmap(mImage, 0, getHeight() - (Math.abs(mTotalY) + getHeight()) % mImage.getHeight(), paint);
            } else {
                canvas.drawBitmap(mImage, 0, mTotalY % mImage.getHeight(), paint);
                canvas.drawBitmap(mImage, 0, -(mImage.getHeight() - mTotalY % mImage.getHeight()), paint);
            }
            // Sprite/Image starting from bottom
          /*  if (mTotalY < 0) {
                canvas.drawBitmap(mImage, 0, h  + mTotalY % mImage.getHeight() - mImage.getHeight(), paint);
                canvas.drawBitmap(mImage, 0, h  + (mTotalY - mImage.getHeight()) % mImage.getHeight(), paint);
            } else {
                canvas.drawBitmap(mImage, 0, h  + mTotalY % mImage.getHeight() - mImage.getHeight(), paint);
                canvas.drawBitmap(mImage, 0, h  - 2 * mImage.getHeight() + mTotalY % mImage.getHeight(), paint);
            }*/
            //canvas.drawBitmap(mImage, 0, mTotalY, paint);
            // canvas.drawBitmap(redRect, clickableStepX, clickableStepY + mTotalY , paint);

            for (int i = currentPosition + 1; i < points.size(); i++) {
                canvas.drawBitmap(unrevealed, points.get(i).getX() - unrevealed.getWidth() / 2, points.get(i).getY() + mTotalY - unrevealed.getHeight() / 2, paint);
            }

                /*for(int i=0; i<points.size();i++){
                    canvas.drawBitmap(unrevealed, points.get(i).getX() - unrevealed.getWidth()/2, points.get(i).getY() - 17250 +  mTotalY - unrevealed.getHeight()/2, paint);
                }*/

            if (giftboxPosition > 0) {
                canvas.drawBitmap(redRect, points.get(giftboxPosition).getX() - redRect.getWidth() / 2, points.get(giftboxPosition).getY() + mTotalY - redRect.getHeight() / 2, paint);
            }
            if (rocketPosition > 0) {
                canvas.drawBitmap(blueRect, points.get(rocketPosition).getX() - redRect.getWidth() / 2, points.get(rocketPosition).getY() + mTotalY - redRect.getHeight() / 2, paint);
            }
            canvas.drawBitmap(pin, points.get(currentPosition).getX() - redRect.getWidth() / 2, points.get(currentPosition).getY() + mTotalY - redRect.getHeight() / 2, paint);
            canvas.drawBitmap(lockedRect, points.get(9).getX() - lockedRect.getWidth() / 2, points.get(9).getY() + mTotalY - lockedRect.getHeight() / 2, paint);


            Paint textPaint = new Paint();
            textPaint.setTextSize(30);
            textPaint.setColor(Color.RED);
            canvas.drawText(mTextField, points.get(9).getX(), points.get(9).getY() + mTotalY - redRect.getHeight() / 2, textPaint);

        }
//        invalidate();

    }

    private void expandPoints() {
        for (int i = 0; i < 100; i++) {
            Pixel p = new Pixel(points.get(i).getX(), points.get(i).getY() - currentPosition / points.size() * 5755);
            points.add(p);
        }
    }

    //on gesture listener implementation; this is first called on fling
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2,
                           final float velocityX, final float velocityY) {

        oldY = 0;
        final float distanceTimeFactor = 0.4f;
        final float totalDx = (distanceTimeFactor * velocityX / 4);
        final float totalDy = (distanceTimeFactor * velocityY / 2);

        onAnimateMove(totalDx, totalDy,
                (long) (800 * distanceTimeFactor));
        return true;
    }


    //region the rest are defaults
    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }
    //endregion

    enum TouchState {UNTOUCHED, IN_TOUCH, START_FLING, IN_FLING}

    ;

    class Touch {
        TouchState state = TouchState.UNTOUCHED;
        /**
         * Where on the view did we initially touch
         */
        final Point viewDown = new Point(0, 0);
        /**
         * What was the coordinates of the viewport origin?
         */
        final Point viewportOriginAtDown = new Point(0, 0);

        final OverScroller scroller;

        TouchThread touchThread;

        Touch(Context context) {
            scroller = new OverScroller(context);
        }

        void start() {
            touchThread = new TouchThread(this);
            touchThread.setName("touchThread");
            touchThread.start();
        }

        boolean down(MotionEvent event) {

            synchronized (this) {
                state = TouchState.IN_TOUCH;

                mStartX = event.getX();
                mStartY = event.getY();
                mCurrentX = event.getRawX();
                mCurrentY = event.getRawY();

                mPinCurrentX = event.getRawX();
                mPinCurrentY = event.getRawY();
            }
            return true;
        }

        boolean move(MotionEvent event) {
            if (state == TouchState.IN_TOUCH) {
                float x = event.getRawX();
                float y = event.getRawY();
                Log.e("Move ", "RAW y: " + y);
                Log.e("Move ", "CURRENT Y: " + mCurrentY);
                // Update how much the touch moved
                mDeltaX = x - mCurrentX;
                mDeltaY = y - mCurrentY;
                Log.e("Move ", "  mDeltaX  " + mDeltaX + "  mDeltaY: " + mDeltaY);
                mCurrentX = x;
                mCurrentY = y;

                mPinDeltaX = x - mPinCurrentX;
                mPinDeltaY = y - mPinCurrentY;

                mPinCurrentX = x;
                mPinCurrentY = y;

                isScrolling = true;
                invalidate();
            }
            return true;
        }

        boolean up(MotionEvent event) {
            if (state == TouchState.IN_TOUCH) {
                state = TouchState.UNTOUCHED;
                mEndX = event.getX();
                mEndY = event.getY();
                if (isAClick(mStartX, mEndX, mStartY, mEndY)) {
                    //Toast.makeText(getContext(), "IT IS A CLICK", Toast.LENGTH_SHORT).show();
                    Log.e("difference x :", "difference x :" + Math.abs(mEndX - clickableStepX));
                    Log.e("difference y :", "difference y :" + Math.abs(mEndY - clickableStepY - mTotalY));
                    float positionDifferenceX = Math.abs(mEndX - clickableStepX);
                    float positionDifferenceY = Math.abs(mEndY - clickableStepY - mTotalY);
                    Log.e("CLICK :  ", "end x:" + mEndX);
                    Log.e("CLICK :  ", "end y:" + mEndY);
                   /* Log.e("CLICK :  " , "next position coords: x: " + points.get(pinPosition+1).getX());
                    Log.e("CLICK :  " , "next position coords: y:" + points.get(pinPosition+1).getY());*/
                   /* if(!(points.get(pinPosition+1).getX()- mEndX > 100*//* =5 *//* || points.get(pinPosition+1).getY()-mEndY > 100)){
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("jump", true);
                        editor.apply();
                        pinPosition++;
                    }*/
                    if (!(Math.abs(mEndX - points.get(rocketPosition).getX()) > 100/* =5 */ || Math.abs(mEndY - points.get(rocketPosition).getY() - mTotalY) > 100)) {
                        ctx.ShowLockedLevelDialog();
                    }
                    if (!(Math.abs(mEndX - points.get(giftboxPosition).getX()) > 100/* =5 */ || Math.abs(mEndY - points.get(giftboxPosition).getY() - mTotalY) > 100)) {
                        ctx.ShowGiftboxDialog();
                    }
                    if (!(Math.abs(mEndX - points.get(9).getX()) > 100/* =5 */ || Math.abs(mEndY - points.get(9).getY() - mTotalY) > 100)) {
                        ctx.showLocked();
                    }
                    if (!(Math.abs(mEndX - points.get(currentPosition).getX()) > 100/* =5 */ || Math.abs(mEndY - points.get(currentPosition).getY() - mTotalY) > 100)) {
                        ctx.ShowScratchableDialog();
                    }
                }
            }
            return true;
        }

        private boolean isAClick(float startX, float endX, float startY, float endY) {
            float differenceX = Math.abs(startX - endX);
            float differenceY = Math.abs(startY - endY);
            return !(differenceX > 5/* =5 */ || differenceY > 5);
        }

        boolean cancel(MotionEvent event) {
            if (state == TouchState.IN_TOUCH) {
                state = TouchState.UNTOUCHED;
            }
            return true;
        }

        class TouchThread extends Thread {
            final Touch touch;
            boolean running = false;

            void setRunning(boolean value) {
                running = value;
            }

            TouchThread(Touch touch) {
                this.touch = touch;
            }

            @Override
            public void run() {
                running = true;
                while (running) {
                    while (touch.state != TouchState.START_FLING && touch.state != TouchState.IN_FLING) {
                        try {
                            Thread.sleep(Integer.MAX_VALUE);
                        } catch (InterruptedException e) {
                        }
                        if (!running)
                            return;
                    }
                    synchronized (touch) {
                        if (touch.state == TouchState.START_FLING) {
                            touch.state = TouchState.IN_FLING;
                        }
                    }
                    if (touch.state == TouchState.IN_FLING) {
                        scroller.computeScrollOffset();
                        //scene.getViewport().setOrigin(scroller.getCurrX(), scroller.getCurrY());
                        Log.e("Thread IN_FLING", "In fling x: " + scroller.getCurrX() + " y: " + scroller.getCurrY());
                        float x = scroller.getCurrX();
                        float y = scroller.getCurrY();

                        // Update how much the touch moved
                        mDeltaY = scroller.getCurrY();

                        mCurrentY += mDeltaY;

                        mPinDeltaY = scroller.getCurrY();

                        mPinCurrentY += mPinDeltaY;

                        isScrolling = true;
                        if (scroller.isFinished()) {
                            //scene.setSuspend(false);
                            synchronized (touch) {
                                touch.state = TouchState.UNTOUCHED;
                                try {
                                    Thread.sleep(5);
                                } catch (InterruptedException e) {
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //used for Fling
    public void onAnimateMove(float dx, float dy, long duration) {
        animateStart = new Matrix(translate);
        animateInterpolator = new DecelerateInterpolator();
        startTime = android.os.SystemClock.elapsedRealtime();
        endTime = startTime + duration;
        totalAnimDy = dy;
        post(new Runnable() {
            @Override
            public void run() {
                onAnimateStep();
            }

        });

    }

    //used for Fling
    private void onAnimateStep() {
        mDeltaY = 0;
        mPinDeltaY = 0;
        long curTime = android.os.SystemClock.elapsedRealtime();
        float percentTime = (float) (curTime - startTime) /
                (float) (endTime - startTime);
        float percentDistance = animateInterpolator
                .getInterpolation(percentTime);
        //float curDx = percentDistance * totalAnimDx;
        float curDy = percentDistance * totalAnimDy;
        if (oldY != 0) {
            mDeltaY = curDy - oldY;
            mPinDeltaY = curDy - oldY;
        }
        Log.e("DELTA POMERAJ", "DELTA POMERAJ" + mDeltaY);
        oldY = curDy;
        Log.e("cur dx", "cur dy" + curDy);

        isScrolling = true;
        invalidate();

        if (percentTime < 1.0f) {
            post(new Runnable() {
                @Override
                public void run() {
                    onAnimateStep();
                }
            });
        }
    }
    //endregion

    private String parseJson() {
        String json = null;
        try {
            InputStream inputStream = getContext().getAssets().open("pixels.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Log.e("onSizeChanged", "height" + h);
        this.w = w;
        this.h = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }
}