package com.sourcey.materiallogindemo.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;


/**
 * Designed specifically to hold ScratchViews and to enable a continuous scratching experience.
 * Created by Bojan on 11/8/2017.
 */

public class ScratchableGridView extends GridLayout {

    public ScratchableGridView(Context context) {
        super(context);
    }

    public ScratchableGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScratchableGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        Timber.d("onInterceptTouchEvent");
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        for (int i = 0; i < getChildCount(); i++) {
            View childView = getChildAt(i);
            if (childView instanceof MegaGigaScratchView) {
                MegaGigaScratchView scratchView = (MegaGigaScratchView) childView;
                if (isTouchInView(scratchView, event.getX(), event.getY())) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        scratchView.stopErase();
//                        Timber.d("STOPPING ERASE");
                    } else {
                        scratchView.erase(event);
                    }
                } else {
                    if (scratchView.isEraseInProgress()) {
                        scratchView.stopErase();
//                        Timber.d("STOPPING ERASE");
                    }
                }
            }
        }

        return true;
    }

    private boolean isTouchInView(View view, float x, float y) {
        if (view.getRight() > x && view.getTop() < y
                && view.getBottom() > y && view.getLeft() < x) {
            return true;
        }

        return false;
    }
}
