package com.sourcey.materiallogindemo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.facebook.FacebookLogin;
import com.sourcey.materiallogindemo.api.login.entities.LoginData;
import com.sourcey.materiallogindemo.api.register.Register;
import com.sourcey.materiallogindemo.api.register.entities.RegisterData;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    @Bind(R.id.input_name) EditText _nameText;
    @Bind(R.id.input_last_name) EditText _lastNameText;
  //  @Bind(R.id.input_address) EditText _addressText;
    @Bind(R.id.input_email) EditText _emailText;
  //  @Bind(R.id.input_mobile) EditText _mobileText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @Bind(R.id.btn_signup) Button _signupButton;
    @Bind(R.id.link_login) TextView _loginLink;
    @Bind(R.id.radioMale) RadioButton _maleRadio;
    @Bind(R.id.radioFemale) RadioButton _femaleRadio;

    CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(SignupActivity.this);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        callbackManager = CallbackManager.Factory.create();

        LoginButton loginButton = (LoginButton) findViewById(R.id.facebook_sign_in_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.setLoginBehavior( LoginBehavior.SUPPRESS_SSO );
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                FacebookLogin fb_login = new FacebookLogin();
                fb_login.login().getUser(loginResult.getAccessToken().getToken()).enqueue(new Callback<LoginData>() {
                    @Override
                    public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                        if (response.body()!=null){
                            onFacebookLoginSuccess();
                            Log.d("PostSuccess", "Access token :  " + response.body().getAccessToken());
                        }
                        else{
                            onFacebookLoginFailed();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginData> call, Throwable t) {
                        Log.d("PostFailure", t.getMessage());
                    }
                });
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        String name = _nameText.getText().toString();
        String last_name = _lastNameText.getText().toString();
        final String email = _emailText.getText().toString();
        final String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();
        String gender = "female";

        if (_maleRadio.isChecked()){
            gender = "male";
        }

        Register register = new Register();

        register.register().SignUpUser(name, last_name, email, password, reEnterPassword, gender).enqueue(new Callback<RegisterData>() {
            @Override
            public void onResponse(Call<RegisterData> call, Response<RegisterData> response) {
                if (response.body() != null){
                    onSignupSuccess(email, password, progressDialog);
                }
                else{
                    progressDialog.hide();
                    onSignupFailed();
                }
            }

            @Override
            public void onFailure(Call<RegisterData> call, Throwable t) {

            }
        });
    }


    public void onSignupSuccess(String email, String password, ProgressDialog progressDialog) {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
//        Toast.makeText(getBaseContext(), "Sign up successful!", Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
//        startActivity(intent);
        LoginActivity la = new LoginActivity();
        la.loginApi(email, password, progressDialog, SignupActivity.this);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Sign up failed!", Toast.LENGTH_LONG).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String last_name = _lastNameText.getText().toString();
                String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 2) {
            _nameText.setError("at least 2 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (last_name.isEmpty() || last_name.length() < 2) {
            _lastNameText.setError("at least 2 characters");
            valid = false;
        } else {
            _lastNameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6) {
            _passwordText.setError("6 or more alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 6 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError("Password Do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }



    public void onFacebookLoginSuccess() {
        Toast.makeText(getBaseContext(), "Facebook login successful!", Toast.LENGTH_LONG).show();
        finish();
    }

    public void onFacebookLoginFailed() {
        Toast.makeText(getBaseContext(), "Facebook login failed", Toast.LENGTH_LONG).show();
    }
}