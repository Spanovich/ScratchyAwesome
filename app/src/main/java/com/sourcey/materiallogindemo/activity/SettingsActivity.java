package com.sourcey.materiallogindemo.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.facebook.FacebookLogin;
import com.sourcey.materiallogindemo.api.login.entities.LoginData;
import com.sourcey.materiallogindemo.api.notifications.NotificationsToggle;
import com.sourcey.materiallogindemo.api.notifications.entities.NotificationsToggleData;
import com.sourcey.materiallogindemo.service.SoundService;

import java.io.IOException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by K-Android 001 on 8/22/2017.
 */

public class SettingsActivity extends AppCompatActivity {

    private final int MAX_VOLUME=11;
//    MediaPlayer music;
    AudioManager audioManager;
    MediaPlayer sound;
    private boolean isMusicOn;
    private boolean isSoundOn;
    private boolean isNotificationOn;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(SettingsActivity.this);
        setContentView(R.layout.activity_settings_new);

        callbackManager = CallbackManager.Factory.create();


        final Button btn_push_notification = (Button) findViewById(R.id.push_notification);
        final ImageView btn_music = (ImageView) findViewById(R.id.music_image);
        final ImageView btn_sound = (ImageView) findViewById(R.id.sound_image);
        Button btn_help = (Button)findViewById(R.id.help_and_support);
        Button btn_changeProfile = (Button)findViewById(R.id.change_profile_settings);
        Button btn_about = (Button) findViewById(R.id.about);


        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);;
//        music = MediaPlayer.create(this, R.raw.test);
//        music.start();
        final SharedPreferences prefs = this.getSharedPreferences("user_info", MODE_PRIVATE);
        isMusicOn = prefs.getBoolean("isMusicOn", true);
        isSoundOn = prefs.getBoolean("isSoundOn", true);
        isNotificationOn = prefs.getBoolean("isNotificationOn", true);

        if(isNotificationOn)
            btn_push_notification.setText("Push notifications | on");
        else
            btn_push_notification.setText("Push notifications | off");

        if (!isMusicOn){
            btn_music.setImageResource(R.drawable.music_sel);
//            music.setVolume(0,0);
        }

        sound = MediaPlayer.create(this, R.raw.test_click);
        if(!isSoundOn) {
            btn_sound.setImageResource(R.drawable.sound_sel);
            sound.setVolume(0,0);
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String deviceToken = sharedPreferences.getString("device_token", "");
        Log.e("device token", "device token "+ deviceToken);

        btn_push_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NotificationsToggle notificationsToggle = new NotificationsToggle(SettingsActivity.this);
                notificationsToggle.notificationToggleService().toggleNotifications().enqueue(new Callback<NotificationsToggleData>() {
                    @Override
                    public void onResponse(Call<NotificationsToggleData> call, Response<NotificationsToggleData> response) {
                        if(response.body()!=null){
                            isNotificationOn = response.body().getData();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean("isNotificationOn", isNotificationOn);
                            editor.commit();
                            if(isNotificationOn)
                                btn_push_notification.setText("Push notifications | on");
                            else
                                btn_push_notification.setText("Push notifications | off");
                        }
                        else{
                            try {
                                Log.d("NotificationToggle", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationsToggleData> call, Throwable t) {

                    }
                });
//                Notification push = new Notification(SettingsActivity.this);
//
//                push.sendToken().pushToken(deviceToken).enqueue(new Callback<ResponseBody>() {
//                    @Override
//                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                        Log.e("response je ", "response : " + response.raw() + response.body() + response.errorBody());
//                        if(response.body() != null){
//                            Toast.makeText(SettingsActivity.this, "Success!", Toast.LENGTH_LONG);
//                            if(isNotificationOn) {
//                                btn_push_notification.setText("Push notifications | off");
//                                SharedPreferences.Editor editor = prefs.edit();
//                                editor.remove("isNotificationOn");
//                                editor.putBoolean("isNotificationOn",false);
//                                editor.commit();
//                                isNotificationOn = false;
//                            }
//                            else{
//                                btn_push_notification.setText("Push notifications | on");
//                                SharedPreferences.Editor editor = prefs.edit();
//                                editor.remove("isNotificationOn");
//                                editor.putBoolean("isNotificationOn",true);
//                                editor.commit();
//                                isNotificationOn = true;
//                            }
//                        }
//                        else{
//                            Toast.makeText(SettingsActivity.this, "An error occured!", Toast.LENGTH_LONG);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//                        Toast.makeText(SettingsActivity.this, "An error occured!", Toast.LENGTH_LONG);
//                    }
//                });
            }
        });

//        music.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mediaPlayer) {
//                music.start();
//            }
//        });

//        btn_music.setImageResource(R.drawable.music_nor);
//        btn_sound.setImageResource(R.drawable.sound_nor);

        btn_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMusicOn){
                    btn_music.setImageResource(R.drawable.music_sel);
//                    music.setVolume(0,0);
                    SoundService.music.setVolume(0,0);
                    isMusicOn = false;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove("isMusicOn");
                    editor.putBoolean("isMusicOn",false);
                    editor.commit();
                }
                else{
                    btn_music.setImageResource(R.drawable.music_nor);
                    SoundService.music.setVolume(100,100);
                    isMusicOn = true;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove("isMusicOn");
                    editor.putBoolean("isMusicOn",true);
                    editor.commit();
                }
            }
        });

        btn_sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSoundOn){
                    btn_sound.setImageResource(R.drawable.sound_sel);
                    sound.setVolume(0,0);
                    isSoundOn = false;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove("isSoundOn");
                    editor.putBoolean("isSoundOn",false);
                    editor.commit();
                }
                else{
                    btn_sound.setImageResource(R.drawable.sound_nor);
                    sound.setVolume(1,1);
                    isSoundOn = true;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove("isSoundOn");
                    editor.putBoolean("isSoundOn",true);
                    editor.commit();
                }
            }
        });


        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sound.start();
            }
        });

        btn_changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SettingsActivity.this, ProfileSettingsActivity.class);
                startActivity(i);
            }
        });



        LoginButton loginButton = (LoginButton) findViewById(R.id.connect_to_facebook);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.setLoginBehavior( LoginBehavior.SUPPRESS_SSO );
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                FacebookLogin fb_login = new FacebookLogin();
                fb_login.login().getUser(loginResult.getAccessToken().getToken()).enqueue(new Callback<LoginData>() {
                    @Override
                    public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                        if (response.body()!=null){
                            Toast.makeText(getBaseContext(), "Facebook login successful!", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(getBaseContext(), "Facebook login failed", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginData> call, Throwable t) {
                        Log.d("PostFailure", t.getMessage());
                    }
                });
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
//        try{
//            if(music != null && !music.isPlaying()){
//                music = MediaPlayer.create(this, R.raw.test);
//                music.start();
//            }
//        }
//        catch (Exception ex){
//            Log.e("MusicError", ex.getMessage());
//        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        if(music != null && music.isPlaying())
//        {
//            music.stop();
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
