package com.sourcey.materiallogindemo.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;

import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.gameplay.GetGiftboxCard;
import com.sourcey.materiallogindemo.api.gameplay.Map;
import com.sourcey.materiallogindemo.api.gameplay.entities.MapData;
import com.sourcey.materiallogindemo.api.gameplay.entities.MapPlacementsData;
import com.sourcey.materiallogindemo.api.gameplay.sevice.GiftboxCardService;
import com.sourcey.materiallogindemo.api.login.Logout;
import com.sourcey.materiallogindemo.api.reset.ResetEmail;
import com.sourcey.materiallogindemo.api.rocket_level.entity.RocketLevelData;
import com.sourcey.materiallogindemo.layout.MegaGigaScratchView;
import com.sourcey.materiallogindemo.layout.ScratchableGridView;
import com.sourcey.materiallogindemo.layout.ScrollImageView;
import com.sourcey.materiallogindemo.notifications.QuickstartPreferences;
import com.sourcey.materiallogindemo.notifications.RegistrationIntentService;
import com.sourcey.materiallogindemo.service.SoundService;
import com.sourcey.materiallogindemo.utils.IabHelper;
import com.sourcey.materiallogindemo.utils.IabResult;
import com.sourcey.materiallogindemo.utils.Inventory;
import com.sourcey.materiallogindemo.utils.Purchase;
import com.sourcey.materiallogindemo.utils.SkuDetails;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class GameplayActivity extends AppCompatActivity {

    //na random je namesteno u onCreate()
    boolean isWinningTicket;

    ScrollImageView gameplayLayout;
    String reward_image ="winning_image";
    String lost_image="losing_image";
    ArrayList<Integer> open_fields;
    HashMap<String, Integer> images;
    ArrayList<String> final_rewards;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private SharedPreferences preferences;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    public static Context ctx;

    float actVolume, maxVolume, volume;
    AudioManager audioManager;
    int soundID;
    int counter = 0;

    boolean plays = false, loaded = false;

    private SoundPool soundPool;
    private Boolean locked;
    private Boolean over;
    private int percentCounter;

    protected PowerManager.WakeLock mWakeLock;

    IabHelper mHelper;
    String base64EncodedPublicKey = "";
    private SharedPreferences settings;
    public static final String ITEM_PURCHASED="ItemPurchased";
    public static final String MONEY="Money";
    public static final int COINS_INAPP_ITEM_1=250;
    public static final int COINS_INAPP_ITEM_2=550;
    // (arbitrary) request code for the purchase flow
    public static final int RC_REQUEST = 10023;
    public static final String PREFS_NAME="Scratchy";

    private Boolean hasNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWinningTicket=true;

        gameplayLayout = new ScrollImageView(this, getStatusBarHeight());

        ctx = GameplayActivity.this;

        setContentView(gameplayLayout);

        Intent svc=new Intent(GameplayActivity.this, SoundService.class);
        startService(svc);

        preferences = getDefaultSharedPreferences(GameplayActivity.this);
        hasNotification = preferences.getBoolean("notification_received",false);

        Bundle bundle= getIntent().getExtras();
        if (bundle!= null) {
            if (getIntent().getExtras().getInt("notification") == 05) {
                showNotificationDialog();
            }
        }

//        if(!prefs.getBoolean("isUserLoggedIn", false)) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
//        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               // mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    Toast.makeText(GameplayActivity.this, getString(R.string.gcm_send_message), Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(GameplayActivity.this, getString(R.string.gcm_send_message), Toast.LENGTH_SHORT).show();
                    //mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
       // mInformationTextView = (TextView) findViewById(R.id.informationTextView);

        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent registrationIntent = new Intent(this, RegistrationIntentService.class);
            startService(registrationIntent);
        }

        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded=true;
            }
        });
        soundID = soundPool.load(GameplayActivity.this, R.raw.scratchnew, 1);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        actVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volume = actVolume / maxVolume;


        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();

        //In App purchases
        settings = getSharedPreferences(PREFS_NAME, 0);
        base64EncodedPublicKey=getResources().getString(R.string.google_base64EncodedPublicKey);

        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG,"Problem setting up in-app billing: " + result);
                    return;
                }
                List<String> additionalSkuList = new ArrayList<String>();
                additionalSkuList.add(getResources().getString(R.string.sku_inapp_250));
                additionalSkuList.add(getResources().getString(R.string.sku_inapp_600));
                //  additionalSkuList.add(getResources().getString(R.string.sku_inapp_levles));
                mHelper.queryInventoryAsync(true, additionalSkuList,
                        mQueryFinishedListener);


            }
        });
        getMap();

    }

    private String giftboxID;

    private void getMap(){
        Map map = new Map(GameplayActivity.this);
        map.mapService().getMap().enqueue(new Callback<MapData>() {
            @Override
            public void onResponse(Call<MapData> call, Response<MapData> response) {
                if (response.body()!=null){
                    ArrayList<MapPlacementsData> placements = response.body().getData().getPlacements();
                    SharedPreferences.Editor editor = preferences.edit();
                    for (int i=0; i<placements.size();i++){
                        MapPlacementsData placement = placements.get(i);
                        switch (placement.getType()) {
                            case "giftbox":
                                Log.d("MapPlacement", "Position: " + placement.getPosition() + "; Type: " + placement.getType() + "; ID: " + placement.getGiftbox().getId() + "; Total Cards: " + placement.getGiftbox().getTotalCards() + ";");
                                giftboxID = placement.getGiftbox().getId();
                                editor.putInt("giftbox", placement.getPosition());
                                editor.apply();
                                break;
                            case "rocket_level":
                                Log.d("MapPlacement", "Position: " + placement.getPosition() + "; Type: " + placement.getType() + "; ID: " + placement.getRocketLevel().getId() + "; Levels: " + placement.getRocketLevel().getLevels() + ";");
                                editor.putInt("rocket", placement.getPosition());
                                editor.apply();
                                break;
                        }
                    }
                }
                else{
                    try {
                        Log.d("MapResponse", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MapData> call, Throwable t) {

            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    @Override
    protected void onDestroy() {
        this.mWakeLock.release();
        stopService(new Intent(this, SoundService.class));
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case R.id.action_wallet:
                Intent intent = new Intent(this, WalletActivity.class);
                startActivity(intent);
                break;
/*            case R.id.action_scratch:
                ShowScratchableDialog();
                break;*/
            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            case R.id.action_giftbox:
                  ShowGiftboxDialog();
                break;
            case R.id.action_locked_level:
                ShowLockedLevelDialog();
                break;
            case R.id.action_confirm_email_change:
                ShowResetEmailConfirmationDialog();
                break;
            case R.id.api_test:
                apitest();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void apitest(){
        GetGiftboxCard getGiftboxCard = new GetGiftboxCard(GameplayActivity.this);
        getGiftboxCard.giftboxCardService().getGiftboxCard(giftboxID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body()!=null){
                    try {
                        Log.d("GetGiftboxCard", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        Log.d("GetGiftboxCard", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    public void ShowLockedLevelDialog(){
        final Dialog dialog = new Dialog(GameplayActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_buy_locked_level);
        Button btnBuy = (Button) dialog.findViewById(R.id.btnBuy);
        final Button btnX = (Button) dialog.findViewById(R.id.btnX);
        final ImageView imgLock = (ImageView) dialog.findViewById(R.id.imgLock);
        final ImageView imgUnlock = (ImageView) dialog.findViewById(R.id.imgUnlock);

        btnX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getDefaultSharedPreferences(GameplayActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                dialog.hide();
            }
        });

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layoutInstruction = (LinearLayout) dialog.findViewById(R.id.layoutInstruction);
                layoutInstruction.setVisibility(View.INVISIBLE);
                btnX.setVisibility(View.INVISIBLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                final boolean[] firstTick = {true};
                new CountDownTimer(1500, 500) {

                    public void onTick(long millisUntilFinished) {
                        if(firstTick[0]) {
                            imgLock.setVisibility(View.VISIBLE);
                            firstTick[0] = false;
                        }
                        else{
                            imgLock.setVisibility(View.GONE);
                            imgUnlock.setVisibility(View.VISIBLE);
                        }
                    }

                    public void onFinish() {
                        /*ArrayList<RocketLevelData> rocketLevelDatas = new ArrayList<RocketLevelData>();
                        rocketLevelDatas.add(new RocketLevelData(false));
                        rocketLevelDatas.add(new RocketLevelData(true));
                        rocketLevelDatas.add(new RocketLevelData(false));
                        rocketLevelDatas.add(new RocketLevelData(false));
                        rocketLevelDatas.add(new RocketLevelData(true));
                        rocketLevelDatas.add(new RocketLevelData(true));
                        rocketLevelDatas.add(new RocketLevelData(false));
                        rocketLevelDatas.add(new RocketLevelData(true));
                        ShowRocketLevelDialog(rocketLevelDatas);*/
                        Intent i = new Intent(GameplayActivity.this, RocketLevelActivity.class);
                        startActivity(i);
                        overridePendingTransition( R.anim.slide_in_up, R.anim.stay );
                        dialog.hide();
                    }
                }.start();
            }
        });
        dialog.show();
    }

    public void showStore(){

    }


    public void ShowGiftboxDialog(){
        final Dialog dialog = new Dialog(GameplayActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_buy_giftbox);

        Button btnBuy = (Button) dialog.findViewById(R.id.btnBuy);
        final Button btnX = (Button) dialog.findViewById(R.id.btnX);
        final ImageView imgGift = (ImageView) dialog.findViewById(R.id.imgGift);
        final ImageView imgGiftOpen = (ImageView) dialog.findViewById(R.id.imgGiftOpen);
        final ImageView imgGiftBoxPreload = (ImageView) dialog.findViewById(R.id.imgGiftBoxPreload);

        btnX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getDefaultSharedPreferences(GameplayActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("jump", true);
                editor.apply();
                dialog.hide();
            }
        });

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layoutInstruction = (LinearLayout) dialog.findViewById(R.id.layoutInstruction);
                layoutInstruction.setVisibility(View.INVISIBLE);
                btnX.setVisibility(View.INVISIBLE);
                imgGift.setVisibility(View.VISIBLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                imgGift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final boolean[] firstTick = {true};
                        imgGift.setVisibility(View.INVISIBLE);
                        new CountDownTimer(1500, 500) {

                            public void onTick(long millisUntilFinished) {
                                if(firstTick[0]) {
                                    imgGiftOpen.setVisibility(View.VISIBLE);
                                    firstTick[0] = false;
                                }
                                else{
                                    imgGiftOpen.setVisibility(View.INVISIBLE);
                                    imgGiftBoxPreload.setVisibility(View.VISIBLE);
                                }
                            }

                            public void onFinish() {
                                ArrayList<RocketLevelData> rocketLevelDatas = new ArrayList<RocketLevelData>();
                                rocketLevelDatas.add(new RocketLevelData(false));
                                rocketLevelDatas.add(new RocketLevelData(true));
                                rocketLevelDatas.add(new RocketLevelData(false));
                                rocketLevelDatas.add(new RocketLevelData(false));
                                rocketLevelDatas.add(new RocketLevelData(true));
                                rocketLevelDatas.add(new RocketLevelData(true));
                                rocketLevelDatas.add(new RocketLevelData(false));
                                rocketLevelDatas.add(new RocketLevelData(true));
                                ShowRocketLevelDialog(rocketLevelDatas);
                                dialog.hide();
                            }
                        }.start();

                    }
                });
            }
        });
        dialog.show();
    }

    private void closeGiftboxDialog(Button btnClose, final Dialog dialog){
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });
    }

    private void setGiftboxNext(final Button btnNext, final ArrayList<RocketLevelData> cards, final Dialog dialog,  final ImageView scratchImage1,
                                 final ImageView scratchImage2,  final ImageView scratchImage3,  final ImageView scratchImage4,
                                 final ImageView scratchImage5,  final ImageView scratchImage6, final MegaGigaScratchView scratchView1,
                                 final MegaGigaScratchView scratchView2, final MegaGigaScratchView scratchView3, final MegaGigaScratchView scratchView4,
                                 final MegaGigaScratchView scratchView5, final MegaGigaScratchView scratchView6, final RelativeLayout relativeLayout2,
                                 final RelativeLayout relativeLayout3, final RelativeLayout relativeLayout4, final RelativeLayout relativeLayout5){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cards.remove(0);
                if (cards.size()>0) {
                    btnNext.setText("Close");
                    closeGiftboxDialog(btnNext, dialog);
                    scratchView1.reset();
                    scratchView1.setVisibility(View.VISIBLE);
                    scratchView2.reset();
                    scratchView2.setVisibility(View.VISIBLE);
                    scratchView3.reset();
                    scratchView3.setVisibility(View.VISIBLE);
                    scratchView4.reset();
                    scratchView4.setVisibility(View.VISIBLE);
                    scratchView5.reset();
                    scratchView5.setVisibility(View.VISIBLE);
                    scratchView6.reset();
                    scratchView6.setVisibility(View.VISIBLE);
                    setRocketLevelData(cards, scratchImage1, scratchImage2, scratchImage3, scratchImage4, scratchImage5, scratchImage6,
                            relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5, scratchView1, scratchView2, scratchView3,
                            scratchView4, scratchView5, scratchView6, btnNext, dialog);
                }
                else {
                    closeGiftboxDialog(btnNext, dialog);
                }

                if (cards.size()==1){
                    btnNext.setText("Close");
                }
            }
        });
    }

    public void ShowRocketLevelDialog(final ArrayList<RocketLevelData> cards){
        final Dialog dialog = new Dialog(GameplayActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_rocket_level);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView scratchImage1 = (ImageView) dialog.findViewById(R.id.scratch_view_behind1);
        ImageView scratchImage2 = (ImageView) dialog.findViewById(R.id.scratch_view_behind2);
        ImageView scratchImage3 = (ImageView) dialog.findViewById(R.id.scratch_view_behind3);
        ImageView scratchImage4 = (ImageView) dialog.findViewById(R.id.scratch_view_behind4);
        ImageView scratchImage5 = (ImageView) dialog.findViewById(R.id.scratch_view_behind5);
        ImageView scratchImage6 = (ImageView) dialog.findViewById(R.id.scratch_view_behind6);

        MegaGigaScratchView scratchView1 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view1);
        MegaGigaScratchView scratchView2 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view2);
        MegaGigaScratchView scratchView3 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view3);
        MegaGigaScratchView scratchView4 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view4);
        MegaGigaScratchView scratchView5 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view5);
        MegaGigaScratchView scratchView6 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view6);

        RelativeLayout relativeLayout2 = (RelativeLayout) dialog.findViewById(R.id.scratch_grid2);
        RelativeLayout relativeLayout3 = (RelativeLayout) dialog.findViewById(R.id.scratch_grid3);
        RelativeLayout relativeLayout4 = (RelativeLayout) dialog.findViewById(R.id.scratch_grid4);
        RelativeLayout relativeLayout5 = (RelativeLayout) dialog.findViewById(R.id.scratch_grid5);

        ScratchableGridView grid = (ScratchableGridView) dialog.findViewById(R.id.gridLayout);

        Button btnNext = (Button) dialog.findViewById(R.id.btnNext);

//        setGiftboxNext(btnNext, cards, dialog, scratchImage1, scratchImage2, scratchImage3, scratchImage4, scratchImage5, scratchImage6,
//                scratchView1, scratchView2, scratchView3, scratchView4, scratchView5, scratchView6, relativeLayout2, relativeLayout3,
//                relativeLayout4, relativeLayout5);

        closeGiftboxDialog(btnNext, dialog);


        images = new HashMap<String, Integer>();
        images.put("winning_image", Integer.valueOf(R.drawable.winning_image));
        images.put("losing_image", Integer.valueOf(R.drawable.losing_image));

        setRocketLevelData(cards, scratchImage1, scratchImage2, scratchImage3, scratchImage4, scratchImage5, scratchImage6,
                relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5, scratchView1, scratchView2, scratchView3,
                scratchView4, scratchView5, scratchView6, btnNext, dialog);

        dialog.show();

//        ShowScratchIntroductionDialog();
    }

    public void setRocketLevelData(final ArrayList<RocketLevelData> cards, final ImageView scratchImage1, final ImageView scratchImage2,
                                   final ImageView scratchImage3, final ImageView scratchImage4, final ImageView scratchImage5, final ImageView scratchImage6,
                                   final RelativeLayout relativeLayout2, final RelativeLayout relativeLayout3, final RelativeLayout relativeLayout4,
                                   final RelativeLayout relativeLayout5, final MegaGigaScratchView scratchView1, final MegaGigaScratchView scratchView2, final MegaGigaScratchView scratchView3,
                                   final MegaGigaScratchView scratchView4, final MegaGigaScratchView scratchView5, final MegaGigaScratchView scratchView6,
                                   final Button btnNext, final Dialog dialog){

        if(cards.size() == 4) {
            relativeLayout5.setVisibility(View.GONE);
        }
        else if (cards.size() == 3){
            relativeLayout5.setVisibility(View.GONE);
            relativeLayout4.setVisibility(View.GONE);
        }
        else if (cards.size() == 2){
            relativeLayout5.setVisibility(View.GONE);
            relativeLayout4.setVisibility(View.GONE);
            relativeLayout3.setVisibility(View.GONE);
        }
        else if (cards.size() == 1){
            relativeLayout5.setVisibility(View.GONE);
            relativeLayout4.setVisibility(View.GONE);
            relativeLayout3.setVisibility(View.GONE);
            relativeLayout2.setVisibility(View.GONE);
        }


        over = false;
        locked = false;
        percentCounter = 0;

        isWinningTicket = cards.get(0).isWinning;
        final_rewards = new ArrayList<String>();
        ArrayList<String> rewards = new ArrayList<String>();
        open_fields = new ArrayList<Integer>();
        rewards.add("winning_image");
        rewards.add("losing_image");

        if (isWinningTicket) {
            for (int i = 0; i < 6; i++) {
                final_rewards.add(reward_image);
            }
        } else {
            do {
                String temp_reward = rewards.get(new Random().nextInt(rewards.size()));
                final_rewards.add(temp_reward);
            } while (final_rewards.size() < 6);
        }

        scratchImage1.setImageResource(images.get(final_rewards.get(0)).intValue());
        scratchImage2.setImageResource(images.get(final_rewards.get(1)).intValue());
        scratchImage3.setImageResource(images.get(final_rewards.get(2)).intValue());
        scratchImage4.setImageResource(images.get(final_rewards.get(3)).intValue());
        scratchImage5.setImageResource(images.get(final_rewards.get(4)).intValue());
        scratchImage6.setImageResource(images.get(final_rewards.get(5)).intValue());

        scratchView1.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent>0){
//                    OnRevealScratchImage(1, scratchView1, scratchView2, scratchView3, scratchView4,
//                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
//                            scratchImage4, scratchImage5, scratchImage6, btnNext);
                    onRevealScratchGiftbox(1, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, dialog, scratchImage5, scratchImage6, btnNext, relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5,
                            cards);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog(btnNext, dialog);
                scratchView1.clear();
                scratchView1.setVisibility(View.INVISIBLE);
            }
        });

        scratchView2.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent>0){
//                    OnRevealScratchImage(2, scratchView1, scratchView2, scratchView3, scratchView4,
//                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
//                            scratchImage4, scratchImage5, scratchImage6, btnNext);
                    onRevealScratchGiftbox(2, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, dialog, scratchImage5, scratchImage6, btnNext, relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5,
                            cards);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog(btnNext, dialog);
                scratchView2.clear();
                scratchView2.setVisibility(View.INVISIBLE);
            }
        });

        scratchView3.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent>0){
//                    OnRevealScratchImage(3, scratchView1, scratchView2, scratchView3, scratchView4,
//                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
//                            scratchImage4, scratchImage5, scratchImage6, btnNext);
                    onRevealScratchGiftbox(3, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, dialog, scratchImage5, scratchImage6, btnNext, relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5,
                            cards);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog(btnNext, dialog);
                scratchView3.clear();
                scratchView3.setVisibility(View.INVISIBLE);
            }
        });

        scratchView4.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent>0){
//                    OnRevealScratchImage(4, scratchView1, scratchView2, scratchView3, scratchView4,
//                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
//                            scratchImage4, scratchImage5, scratchImage6, btnNext);
                    onRevealScratchGiftbox(4, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, dialog, scratchImage5, scratchImage6, btnNext, relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5,
                            cards);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog(btnNext, dialog);
                scratchView4.clear();
                scratchView4.setVisibility(View.INVISIBLE);
            }
        });

        scratchView5.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent>0){
//                    OnRevealScratchImage(5, scratchView1, scratchView2, scratchView3, scratchView4,
//                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
//                            scratchImage4, scratchImage5, scratchImage6, btnNext);
                    onRevealScratchGiftbox(5, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, dialog, scratchImage5, scratchImage6, btnNext, relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5,
                            cards);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog(btnNext, dialog);
                scratchView5.clear();
                scratchView5.setVisibility(View.INVISIBLE);
            }
        });

        scratchView6.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent>0){
//                    OnRevealScratchImage(6, scratchView1, scratchView2, scratchView3, scratchView4,
//                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
//                            scratchImage4, scratchImage5, scratchImage6, btnNext);
                    onRevealScratchGiftbox(6, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, dialog, scratchImage5, scratchImage6, btnNext, relativeLayout2, relativeLayout3, relativeLayout4, relativeLayout5,
                            cards);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog(btnNext, dialog);
                scratchView6.clear();
                scratchView6.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void ShowResetEmailConfirmationDialog(){
        final Dialog dialog = new Dialog(GameplayActivity.this);
        dialog.setContentView(R.layout.dialog_reset_email_confirmation);
        dialog.setTitle("New email address confirmation");

        final EditText code_text = (EditText) dialog.findViewById(R.id.input_code);
        Button btnSubmitCode = (Button) dialog.findViewById(R.id.btnSubmit);

        btnSubmitCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = code_text.getText().toString();
                ResetEmail resetEmail = new ResetEmail();
                resetEmail.resetEmailConformation().ResetEmailConfirmation(code).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.errorBody() == null){
                            Toast.makeText(GameplayActivity.this, "Success reset email!", Toast.LENGTH_LONG);
                            Logout logout = new Logout(GameplayActivity.this);
                            logout.logout().Logout();
                            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                            startActivity(intent);
                            dialog.hide();
                            finish();
                        }
                        else{
                            Toast.makeText(GameplayActivity.this, "Error reset email!", Toast.LENGTH_LONG);
                            dialog.hide();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(GameplayActivity.this, "Error reset email!", Toast.LENGTH_LONG);
                        dialog.hide();
                    }
                });
            }
        });

        dialog.show();
    }

    public void showNotificationDialog(){
        String message = preferences.getString("notification_message", " ");
        final Dialog dialog = new Dialog(GameplayActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notification);
        Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });
        TextView messageText = (TextView) dialog.findViewById(R.id.message);
        messageText.setText(message);
        dialog.show();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("notification_received", false);
        editor.commit();
    }

    public void ShowScratchableDialog() {
//        IsWinningCard isWinningCard = new IsWinningCard(GameplayActivity.this);
//        isWinningCard.isWinningCard().getIsWinningCard().enqueue(new Callback<IsWinningCardDataPrime>() {
//
//            @Override
//            public void onResponse(Call<IsWinningCardDataPrime> call, Response<IsWinningCardDataPrime> response) {
//                if (response.body() != null) {
//
//                    isWinningTicket = response.body().data.getWin();

//                    isWinningTicket = true;
                    SharedPreferences preferences = getDefaultSharedPreferences(GameplayActivity.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("discard", false);
                    editor.apply();

                    Random random = new Random();
                    isWinningTicket = random.nextBoolean();

                    final Dialog dialog = new Dialog(GameplayActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_scratch);

                    over = false;
                    locked = false;
                    percentCounter = 0;

                    final ImageView scratchImage1 = (ImageView) dialog.findViewById(R.id.scratch_view_behind1);
                    final ImageView scratchImage2 = (ImageView) dialog.findViewById(R.id.scratch_view_behind2);
                    final ImageView scratchImage3 = (ImageView) dialog.findViewById(R.id.scratch_view_behind3);
                    final ImageView scratchImage4 = (ImageView) dialog.findViewById(R.id.scratch_view_behind4);
                    final ImageView scratchImage5 = (ImageView) dialog.findViewById(R.id.scratch_view_behind5);
                    final ImageView scratchImage6 = (ImageView) dialog.findViewById(R.id.scratch_view_behind6);

                    final MegaGigaScratchView scratchView1 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view1);
                    final MegaGigaScratchView scratchView2 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view2);
                    final MegaGigaScratchView scratchView3 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view3);
                    final MegaGigaScratchView scratchView4 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view4);
                    final MegaGigaScratchView scratchView5 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view5);
                    final MegaGigaScratchView scratchView6 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view6);


                    final Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            SharedPreferences preferences = getDefaultSharedPreferences(GameplayActivity.this);
                            SharedPreferences.Editor editor = preferences.edit();
                            if(dialogButton.getText().equals("Discard")){
                                editor.putBoolean("discard", true);
                                editor.apply();
                            }
                            if(dialogButton.getText().equals("Discard") || open_fields.size() == 6) {
                                editor.putBoolean("jump", true);
                                editor.apply();
                            }

                        }
                    });

                    images = new HashMap<String, Integer>();
                    images.put("winning_image", Integer.valueOf(R.drawable.winning_image));
                    images.put("losing_image", Integer.valueOf(R.drawable.losing_image));

                    final_rewards = new ArrayList<String>();
                    open_fields = new ArrayList<Integer>();
                    ArrayList<String> rewards = new ArrayList<String>();

                    rewards.add("winning_image");
                    rewards.add("losing_image");

                    if (isWinningTicket) {
                        for (int i = 0; i < 6; i++) {
                            final_rewards.add(reward_image);
                        }
                    } else {
                        do {
                            String temp_reward = rewards.get(new Random().nextInt(rewards.size()));
                            final_rewards.add(temp_reward);
                        } while (final_rewards.size() < 6);
                    }

                    scratchImage1.setImageResource(images.get(final_rewards.get(0)).intValue());
                    scratchImage2.setImageResource(images.get(final_rewards.get(1)).intValue());
                    scratchImage3.setImageResource(images.get(final_rewards.get(2)).intValue());
                    scratchImage4.setImageResource(images.get(final_rewards.get(3)).intValue());
                    scratchImage5.setImageResource(images.get(final_rewards.get(4)).intValue());
                    scratchImage6.setImageResource(images.get(final_rewards.get(5)).intValue());

                    scratchView1.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
                        @Override
                        public void onProgress(int percent) {
                            if (percent > 0) {
                                OnRevealScratchImage(1, scratchView1, scratchView2, scratchView3, scratchView4,
                                        scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                                        scratchImage4, scratchImage5, scratchImage6, dialogButton);
                            }
                        }

                        @Override
                        public void onCompleted(View view) {
                            percentCounter++;
                            showWinningDialog(dialogButton, dialog);
                            scratchView1.clear();
                            scratchView1.setVisibility(View.INVISIBLE);
                        }
                    });

                    scratchView2.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
                        @Override
                        public void onProgress(int percent) {
                            if (percent > 0) {
                                OnRevealScratchImage(2, scratchView1, scratchView2, scratchView3, scratchView4,
                                        scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                                        scratchImage4, scratchImage5, scratchImage6, dialogButton);
                            }
                        }

                        @Override
                        public void onCompleted(View view) {
                            percentCounter++;
                            showWinningDialog(dialogButton, dialog);
                            scratchView2.clear();
                            scratchView2.setVisibility(View.INVISIBLE);
                        }
                    });

                    scratchView3.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
                        @Override
                        public void onProgress(int percent) {
                            if (percent > 0) {
                                OnRevealScratchImage(3, scratchView1, scratchView2, scratchView3, scratchView4,
                                        scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                                        scratchImage4, scratchImage5, scratchImage6, dialogButton);
                            }
                        }

                        @Override
                        public void onCompleted(View view) {
                            percentCounter++;
                            showWinningDialog(dialogButton, dialog);
                            scratchView3.clear();
                            scratchView3.setVisibility(View.INVISIBLE);
                        }
                    });

                    scratchView4.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
                        @Override
                        public void onProgress(int percent) {
                            if (percent > 0) {
                                OnRevealScratchImage(4, scratchView1, scratchView2, scratchView3, scratchView4,
                                        scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                                        scratchImage4, scratchImage5, scratchImage6, dialogButton);
                            }
                        }

                        @Override
                        public void onCompleted(View view) {
                            percentCounter++;
                            showWinningDialog(dialogButton, dialog);
                            scratchView4.clear();
                            scratchView4.setVisibility(View.INVISIBLE);
                        }
                    });

                    scratchView5.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
                        @Override
                        public void onProgress(int percent) {
                            if (percent > 0) {
                                OnRevealScratchImage(5, scratchView1, scratchView2, scratchView3, scratchView4,
                                        scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                                        scratchImage4, scratchImage5, scratchImage6, dialogButton);
                            }
                        }

                        @Override
                        public void onCompleted(View view) {
                            percentCounter++;
                            showWinningDialog(dialogButton, dialog);
                            scratchView5.clear();
                            scratchView5.setVisibility(View.INVISIBLE);
                        }
                    });

                    scratchView6.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
                        @Override
                        public void onProgress(int percent) {
                            if (percent > 0) {
                                OnRevealScratchImage(6, scratchView1, scratchView2, scratchView3, scratchView4,
                                        scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                                        scratchImage4, scratchImage5, scratchImage6, dialogButton);
                            }
                        }

                        @Override
                        public void onCompleted(View view) {
                            percentCounter++;
                            showWinningDialog(dialogButton, dialog);
                            scratchView6.clear();
                            scratchView6.setVisibility(View.INVISIBLE);
                        }
                    });

                    dialog.show();

                    ShowScratchIntroductionDialog();

//                } else {
//                    Log.d("IsWinningCard", response.errorBody().toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<IsWinningCardDataPrime> call, Throwable t) {
//
//            }
//        });
    }


    private void ShowScratchIntroductionDialog(){
        SharedPreferences prefs = ctx.getSharedPreferences("user_info", MODE_PRIVATE);
        if(prefs.getBoolean("show_scratch_instructions", true)) {
            final Dialog dialog = new Dialog(GameplayActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_scratch_introduction);
            Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
            btnContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.hide();
                }
            });
            dialog.show();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("show_scratch_instructions", false);
            editor.commit();
        }
    }

    private void onRevealScratchGiftbox(int ScratchNo, MegaGigaScratchView scratchView1, MegaGigaScratchView scratchView2, MegaGigaScratchView scratchView3,
                                        MegaGigaScratchView scratchView4, MegaGigaScratchView scratchView5, MegaGigaScratchView scratchView6,
                                        ImageView imageView1, ImageView imageView2, ImageView imageView3, ImageView imageView4, Dialog dialog,
                                        ImageView imageView5, ImageView imageView6, Button btnClose, RelativeLayout relativeLayout2,
                                        RelativeLayout relativeLayout3, RelativeLayout relativeLayout4, RelativeLayout relativeLayout5,
                                        final ArrayList<RocketLevelData> cards){
        if (!open_fields.contains(ScratchNo)) {
            open_fields.add(ScratchNo);

            if (open_fields.size()==1) {
                btnClose.setText("Discard");
                setGiftboxNext(btnClose, cards, dialog, imageView1, imageView2, imageView3, imageView4, imageView5, imageView6, scratchView1,
                        scratchView2, scratchView3, scratchView4, scratchView5, scratchView6, relativeLayout2, relativeLayout3, relativeLayout4,
                        relativeLayout5);
            }

            if (open_fields.size() == 2 && !isWinningTicket) {
                for (int i = 1; i < 7; i++) {
                    if (!open_fields.contains(i)) {
                        switch (i) {
                            case 1:
                                imageView1.setImageResource(images.get(lost_image));
                                break;
                            case 2:
                                imageView2.setImageResource(images.get(lost_image));
                                break;
                            case 3:
                                imageView3.setImageResource(images.get(lost_image));
                                break;
                            case 4:
                                imageView4.setImageResource(images.get(lost_image));
                                break;
                            case 5:
                                imageView5.setImageResource(images.get(lost_image));
                                break;
                            case 6:
                                imageView6.setImageResource(images.get(lost_image));
                                break;
                        }
                    }

                }
            }

            if(open_fields.size()==3){
                LockUnreaveledFields(scratchView1, scratchView2, scratchView3, scratchView4, scratchView5,
                        scratchView6, imageView1, imageView2, imageView3, imageView4, imageView5, imageView6);
            }
        }
    }

    private void OnRevealScratchImage(int ScratchNo, MegaGigaScratchView scratchView1, MegaGigaScratchView scratchView2, MegaGigaScratchView scratchView3,
                                      MegaGigaScratchView scratchView4, MegaGigaScratchView scratchView5, MegaGigaScratchView scratchView6,
                                      ImageView imageView1, ImageView imageView2, ImageView imageView3, ImageView imageView4,
                                      ImageView imageView5, ImageView imageView6, Button btnClose)
    {
       playSound();
        if (!open_fields.contains(ScratchNo)) {
            open_fields.add(ScratchNo);

            if (open_fields.size()==1) {
                btnClose.setText("Discard");
            }

            if (open_fields.size() == 2 && !isWinningTicket) {
                for (int i = 1; i < 7; i++) {
                    if (!open_fields.contains(i)) {
                        switch (i) {
                            case 1:
                                imageView1.setImageResource(images.get(lost_image));
                                break;
                            case 2:
                                imageView2.setImageResource(images.get(lost_image));
                                break;
                            case 3:
                                imageView3.setImageResource(images.get(lost_image));
                                break;
                            case 4:
                                imageView4.setImageResource(images.get(lost_image));
                                break;
                            case 5:
                                imageView5.setImageResource(images.get(lost_image));
                                break;
                            case 6:
                                imageView6.setImageResource(images.get(lost_image));
                                break;
                        }
                    }

                }
            }

            if(open_fields.size()==3){
                LockUnreaveledFields(scratchView1, scratchView2, scratchView3, scratchView4, scratchView5,
                        scratchView6, imageView1, imageView2, imageView3, imageView4, imageView5, imageView6);
            }
        }
    }

    private void LockUnreaveledFields(MegaGigaScratchView scratchView1, MegaGigaScratchView scratchView2, MegaGigaScratchView scratchView3,
                                      MegaGigaScratchView scratchView4, MegaGigaScratchView scratchView5, MegaGigaScratchView scratchView6,
                                      ImageView imageView1, ImageView imageView2, ImageView imageView3, ImageView imageView4,
                                      ImageView imageView5, ImageView imageView6){
        locked = true;
        over=true;
        if(!open_fields.contains(1)){
            imageView1.setImageResource(R.drawable.lock);
            scratchView1.setVisibility(View.INVISIBLE);
            scratchView1.clear();
        }
        if(!open_fields.contains(2)){
            imageView2.setImageResource(R.drawable.lock);
            scratchView2.setVisibility(View.INVISIBLE);
            scratchView2.clear();
        }
        if(!open_fields.contains(3)){
            imageView3.setImageResource(R.drawable.lock);
            scratchView3.setVisibility(View.INVISIBLE);
            scratchView3.clear();
        }
        if(!open_fields.contains(4)){
            imageView4.setImageResource(R.drawable.lock);
            scratchView4.setVisibility(View.INVISIBLE);
            scratchView4.clear();
        }
        if(!open_fields.contains(5)){
            imageView5.setImageResource(R.drawable.lock);
            scratchView5.setVisibility(View.INVISIBLE);
            scratchView5.clear();
        }
        if(!open_fields.contains(6)) {
            imageView6.setImageResource(R.drawable.lock);
            scratchView6.setVisibility(View.INVISIBLE);
            scratchView6.clear();
        }

    }

    private void showWinningDialog(Button btn_close, Dialog closeDialog) {
        Log.e("Show winning dialog ", "percent counter : " + percentCounter);
        if(open_fields.size() == 6 && percentCounter == 6) {
            over=true;
            btn_close.setText("Close");
//            closeGiftboxDialog(btn_close, closeDialog);
            if(isWinningTicket) {
                final Dialog dialog = new Dialog(GameplayActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_win);

                Button redeem = (Button) dialog.findViewById(R.id.redeem_button);
                Button later = (Button) dialog.findViewById(R.id.later_btn);
                Button share_fb_btn = (Button) dialog.findViewById(R.id.share_facebook_button);

                redeem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(GameplayActivity.this, WalletActivity.class);
                        startActivity(intent);
                    }
                });

                later.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                share_fb_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.bunny_scratch.fl"))
                                .build();
                        ShareDialog shareDialog = new ShareDialog(GameplayActivity.this);
                        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                    }
                });

                dialog.show();
            }
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void playSound() {
        if(!over) {
            // Is the sound loaded does it already play?
            synchronized (GameplayActivity.this) {
                if (loaded && !plays) {
                    soundPool.play(soundID, volume, volume, 1, 0, 1f);
                    counter = counter++;
                    //Toast.makeText(this, "Played sound", Toast.LENGTH_SHORT).show();
                    plays = true;
                }
            }
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    plays = false;
                    soundID = soundPool.load(ctx, R.raw.scratchnew, counter);
                }
            }, 1000);
        }
    }

    public void showLocked(){
        final Dialog dialog = new Dialog(GameplayActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_buy_locked_level);
        Button btnBuy = (Button) dialog.findViewById(R.id.btnBuy);
        final Button btnX = (Button) dialog.findViewById(R.id.btnX);

        btnX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(GameplayActivity.this, WalletActivity.class);
                startActivity(i);
                dialog.cancel();*/
                buyCoins(getResources().getString(R.string.sku_inapp_600));
            }
        });
        dialog.show();
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (mHelper == null) return;
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
            if (result.isFailure()) {
                Log.d(TAG,"Purchase failed");
              //  setWaitScreen(false);
                return;
            }
           /* if (!verifyDeveloperPayload(purchase)) {
                Log.d(TAG,"Error purchasing. Authenticity verification failed.");
                setWaitScreen(false);
                return;
            }*/

            Log.d(TAG, "Purchase successful.");
            Log.d(TAG, "purchase sku "+purchase.getSku());
            //if sku
            if (purchase.getSku().equals(getResources().getString(R.string.sku_inapp_levles))||purchase.getSku().equals(getResources().getString(R.string.sku_inapp_250))||purchase.getSku().equals(getResources().getString(R.string.sku_inapp_600))) {
                //consume it
                Log.d(TAG, "Starting  consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }

            //InitLib.getInstance(InAppActivity.this).reportIap();
			/*switch (purchase.getSku()) {
				case "smartriddles250coins":
					InitLib.getInstance(InAppActivity.this).reportIap(purchase.getSku(), "EUR", 980000);
					break;
				case "smartriddles600coins":
					InitLib.getInstance(InAppActivity.this).reportIap(purchase.getSku(), "EUR", 1870000);
					break;
				case "smartriddlesadditionallevels":
					InitLib.getInstance(InAppActivity.this).reportIap(purchase.getSku(), "EUR", 2240000);
					break;

			}*/

        }


    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
            if (mHelper == null) return;
            //check sku
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                String sku=purchase.getSku();
                //increment coins
                int coins;
                if(sku.equalsIgnoreCase(getResources().getString(R.string.sku_inapp_250))){
                    setItemPurchased();
                    coins = modifyMoney(COINS_INAPP_ITEM_1);
                    //txtStatusIap.setText(getResources().getString(R.string.txt_status_iap_1)+" "+coins+" "+getResources().getString(R.string.txt_status_iap_2));
                }else if(sku.equalsIgnoreCase(getResources().getString(R.string.sku_inapp_600))){
                    setItemPurchased();
                    coins = modifyMoney(COINS_INAPP_ITEM_2);
                   // txtStatusIap.setText(getResources().getString(R.string.txt_status_iap_1)+" "+coins+" "+getResources().getString(R.string.txt_status_iap_2));
                    //}else if(sku.equalsIgnoreCase(getResources().getString(R.string.sku_inapp_levles))){
                    //	setItemPurchased();

                    //	//txtStatusIap.setText(getResources().getString(R.string.txt_status_iap_1)+" "+coins+" "+getResources().getString(R.string.txt_status_iap_2));
                }
            }
            else {
                Log.e(TAG,"Error while consuming: " + result);
            }

           // setWaitScreen(false);
            Log.d(TAG, "End consumption flow.");
        }


    };

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");
            if (mHelper == null) return;
            if (result.isFailure()) {
                Log.d(TAG,"Failed to query inventory: " + result);
                //setWaitScreen(false);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");
            Log.d(TAG, "Check."+ getResources().getString(R.string.sku_inapp_250)+" "+inventory.hasPurchase(getResources().getString(R.string.sku_inapp_250)));
            Log.d(TAG, "Check."+ getResources().getString(R.string.sku_inapp_600)+" "+inventory.hasPurchase(getResources().getString(R.string.sku_inapp_600)));
            //  Log.d(TAG, "Check."+ getResources().getString(R.string.sku_inapp_levles)+" "+inventory.hasPurchase(getResources().getString(R.string.sku_inapp_levles)));


            // Check for coins bought and process them immediately
            Purchase coins100Purchase = inventory.getPurchase(getResources().getString(R.string.sku_inapp_250));
            if (coins100Purchase != null && verifyDeveloperPayload(coins100Purchase)) {
                Log.d(TAG, "We have 100 coins purchase. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(getResources().getString(R.string.sku_inapp_250)), mConsumeFinishedListener);
                return;
            }
            // Check for coins bought and process them immediately
            Purchase coins300Purchase = inventory.getPurchase(getResources().getString(R.string.sku_inapp_600));
            if (coins100Purchase != null && verifyDeveloperPayload(coins300Purchase)) {
                Log.d(TAG, "We have 300 coins purchase. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(getResources().getString(R.string.sku_inapp_600)), mConsumeFinishedListener);
                return;
            }
            // Check for coins bought and process them immediately
            ///   Purchase levesPurchase = inventory.getPurchase(getResources().getString(R.string.sku_inapp_levles));
            // if (coins100Purchase != null && verifyDeveloperPayload(levesPurchase)) {
            //    Log.d(TAG, "We have levels coins purchase. Consuming it.");
            //   mHelper.consumeAsync(inventory.getPurchase(getResources().getString(R.string.sku_inapp_levles)), mConsumeFinishedListener);
            //     return;
            //  }

           // setWaitScreen(false);
            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    // Listener that's called when we finish querying for items details
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            Log.d(TAG, "Query sku items finished.");
            if (mHelper == null) return;
            if (result.isFailure()) {
                Log.d(TAG,"Failed to query inventory: " + result);
                //setWaitScreen(false);
                return;
            }

            Log.d(TAG, "Query sku items was successful.");

            SkuDetails coins100 =
                    inventory.getSkuDetails(getResources().getString(R.string.sku_inapp_250));

            if(coins100!=null) {
                Log.d(TAG, "sku available coins100 " +coins100.getDescription()+" "+coins100.toString());
               // enableSkuInterface(coins100);
            }
            SkuDetails coins300 =
                    inventory.getSkuDetails(getResources().getString(R.string.sku_inapp_600));

            if(coins300!=null) {
                Log.d(TAG, "sku available coins300 " +coins300.getDescription()+" "+coins300.toString());
               // enableSkuInterface(coins300);
            }
            SkuDetails levels_aditioanal =
                    inventory.getSkuDetails(getResources().getString(R.string.sku_inapp_levles));

            //  if(levels_aditioanal!=null) {
            //   	 Log.d(TAG, "sku available levels_additional " +levels_aditioanal.getDescription()+" "+levels_aditioanal.toString());
            //   	 if(!inventory.hasPurchase(getResources().getString(R.string.sku_inapp_levles))){
            //  	enableSkuInterface(levels_aditioanal);}
            //  	 else {
            //  		 settings = getSharedPreferences(PREFS_NAME, 0);
            //			SharedPreferences.Editor editor = settings.edit();
            //			editor.putBoolean(ADDITIONAL_UNLOCKED, true);
            //			editor.commit();
            //  	 }
            //   }

            // setWaitScreen(false);
            Log.d(TAG, "Initial sku items query finished; enabling main UI.");
            // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
            Log.d(TAG, "Querying inventory.");

            mHelper.queryInventoryAsync(mGotInventoryListener);
        }
    };

    public void setItemPurchased() {

        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ITEM_PURCHASED, true);
        editor.commit();
        return;
    }

    private boolean verifyDeveloperPayload(Purchase purchase) {
        String payload = settings.getString(purchase.getSku(), "");
		/*
		if(payload.equalsIgnoreCase(purchase.getDeveloperPayload())){
			return true;
		}else{
			return false;
		}
		*/
        return true;

    }
    public int modifyMoney(int value) {

        int money = settings.getInt(MONEY, 0);
        money = money + value;
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(MONEY, money);
        editor.commit();
        return money;
    }

    protected void buyCoins(String sku) {
      //  setWaitScreen(true);
        String payload ="";// UUID.randomUUID().toString(); //generate string for crt user
        SharedPreferences.Editor editor=settings.edit();
        //TODO getUser
        try{
            mHelper.launchPurchaseFlow(this, sku, RC_REQUEST,
                    mPurchaseFinishedListener, payload);
        }catch(IllegalStateException ise){
            //just wait to finish other tasks
            Toast.makeText(this, R.string.wait_message, Toast.LENGTH_SHORT).show();
         //   setWaitScreen(false);
            Log.e(TAG,""+ ise.getMessage());
        }
        editor.putString(sku, payload);
        editor.commit();
    }
}
