package com.sourcey.materiallogindemo.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.fragment.Tab1;
import com.sourcey.materiallogindemo.fragment.Tab2;
import com.sourcey.materiallogindemo.fragment.Tab3;
import com.sourcey.materiallogindemo.fragment.WalletStoreFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lokal on 8/19/2017.
 */

public class WalletActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private FloatingActionButton fab;
    private TabLayout mTabLayout;
    private ImageView backbutton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        backbutton = (ImageView) findViewById(R.id.back_btn);

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        setupViewPager(viewPager);
        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(viewPager);
        }
        viewPager.setCurrentItem(2);
        setupTabHeaders();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Tab1 mTab1 = new Tab1();
        adapter.addFragment(mTab1);
        Tab2 mTab2 = new Tab2();
        adapter.addFragment(mTab2);

        WalletStoreFragment mTab3 = WalletStoreFragment.newInstance();
        adapter.addFragment(mTab3);

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter

    {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

        @Override
        public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

        @Override
        public int getCount() {
        return mFragmentList.size();
    }

    void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
    }
    private void setupTabHeaders() {
        try {
            mTabLayout.getTabAt(0).setText(R.string.tab1header);
            mTabLayout.getTabAt(1).setText(R.string.tab2header);
            mTabLayout.getTabAt(2).setText(R.string.tab3header);
        } catch (NullPointerException e) {
            //
        }
    }



}
