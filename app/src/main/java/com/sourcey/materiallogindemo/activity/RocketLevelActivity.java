package com.sourcey.materiallogindemo.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.preference.PreferenceManager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.gameplay.GetCard;
import com.sourcey.materiallogindemo.api.gameplay.entities.IsWinningCardDataPrime;
import com.sourcey.materiallogindemo.layout.MegaGigaScratchView;
import com.sourcey.materiallogindemo.layout.RocketLevelImageView;
import com.sourcey.materiallogindemo.service.SoundService;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RocketLevelActivity extends AppCompatActivity {

    //na random je namesteno u onCreate()
    boolean isWinningTicket;

    RocketLevelImageView gameplayLayout;
    String reward_image ="winning_image";
    String lost_image="losing_image";
    ArrayList<Integer> open_fields;
    HashMap<String, Integer> images;
    ArrayList<String> final_rewards;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    public static Context ctx;

    float actVolume, maxVolume, volume;
    AudioManager audioManager;
    int soundID;
    int counter = 0;

    boolean plays = false, loaded = false;

    private SoundPool soundPool;
    private Boolean locked;
    private Boolean over;
    private int percentCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gameplayLayout = new RocketLevelImageView(this);

        ctx = RocketLevelActivity.this;
        setContentView(gameplayLayout);


        // mInformationTextView = (TextView) findViewById(R.id.informationTextView);


        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded=true;
            }
        });
        soundID = soundPool.load(RocketLevelActivity.this, R.raw.scratchnew, 1);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        actVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volume = actVolume / maxVolume;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, SoundService.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    public void ShowScratchableDialog() {
//        IsWinningCard isWinningCard = new IsWinningCard(GameplayActivity.this);
//        isWinningCard.isWinningCard().getIsWinningCard().enqueue(new Callback<IsWinningCardDataPrime>() {
//
//            @Override
//            public void onResponse(Call<IsWinningCardDataPrime> call, Response<IsWinningCardDataPrime> response) {
//                if (response.body() != null) {
//
//                    isWinningTicket = response.body().data.getWin();

    //    isWinningTicket = true;

        final Dialog dialog = new Dialog(RocketLevelActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_scratch);

        over = false;
        locked = false;
        percentCounter = 0;


        GetCard isWinningCard = new GetCard(RocketLevelActivity.this);

        isWinningCard.isWinningCard().getIsWinningCard().enqueue(new Callback<IsWinningCardDataPrime>() {

            @Override
            public void onResponse(Call<IsWinningCardDataPrime> call, Response<IsWinningCardDataPrime> response) {
                if(response.body()!=null) {
                    isWinningTicket = response.body().data.getWin();
//                    try {
//                        Log.d("IsWinningCard", response.body().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                }
                else {
                    Log.d("IsWinningCard", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<IsWinningCardDataPrime> call, Throwable t) {

            }
        });

        Random random = new Random();
        isWinningTicket = random.nextBoolean();

        final ImageView scratchImage1 = (ImageView) dialog.findViewById(R.id.scratch_view_behind1);
        final ImageView scratchImage2 = (ImageView) dialog.findViewById(R.id.scratch_view_behind2);
        final ImageView scratchImage3 = (ImageView) dialog.findViewById(R.id.scratch_view_behind3);
        final ImageView scratchImage4 = (ImageView) dialog.findViewById(R.id.scratch_view_behind4);
        final ImageView scratchImage5 = (ImageView) dialog.findViewById(R.id.scratch_view_behind5);
        final ImageView scratchImage6 = (ImageView) dialog.findViewById(R.id.scratch_view_behind6);

        final MegaGigaScratchView scratchView1 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view1);
        final MegaGigaScratchView scratchView2 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view2);
        final MegaGigaScratchView scratchView3 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view3);
        final MegaGigaScratchView scratchView4 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view4);
        final MegaGigaScratchView scratchView5 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view5);
        final MegaGigaScratchView scratchView6 = (MegaGigaScratchView) dialog.findViewById(R.id.scratch_view6);


        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(RocketLevelActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("jump", true);
                editor.apply();

            }
        });

        images = new HashMap<String, Integer>();
        images.put("winning_image", Integer.valueOf(R.drawable.winning_image));
        images.put("losing_image", Integer.valueOf(R.drawable.losing_image));

        final_rewards = new ArrayList<String>();
        open_fields = new ArrayList<Integer>();
        ArrayList<String> rewards = new ArrayList<String>();

        rewards.add("winning_image");
        rewards.add("losing_image");

        if (isWinningTicket) {
            for (int i = 0; i < 6; i++) {
                final_rewards.add(reward_image);
            }
        } else {
            do {
                String temp_reward = rewards.get(new Random().nextInt(rewards.size()));
                final_rewards.add(temp_reward);
            } while (final_rewards.size() < 6);
        }

        scratchImage1.setImageResource(images.get(final_rewards.get(0)).intValue());
        scratchImage2.setImageResource(images.get(final_rewards.get(1)).intValue());
        scratchImage3.setImageResource(images.get(final_rewards.get(2)).intValue());
        scratchImage4.setImageResource(images.get(final_rewards.get(3)).intValue());
        scratchImage5.setImageResource(images.get(final_rewards.get(4)).intValue());
        scratchImage6.setImageResource(images.get(final_rewards.get(5)).intValue());

        scratchView1.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent > 0) {
                    OnRevealScratchImage(1, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, scratchImage5, scratchImage6);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog();
                scratchView1.clear();
                scratchView1.setVisibility(View.INVISIBLE);
            }
        });

        scratchView2.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent > 0) {
                    OnRevealScratchImage(2, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, scratchImage5, scratchImage6);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog();
                scratchView2.clear();
                scratchView2.setVisibility(View.INVISIBLE);
            }
        });

        scratchView3.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent > 0) {
                    OnRevealScratchImage(3, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, scratchImage5, scratchImage6);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog();
                scratchView3.clear();
                scratchView3.setVisibility(View.INVISIBLE);
            }
        });

        scratchView4.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent > 0) {
                    OnRevealScratchImage(4, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, scratchImage5, scratchImage6);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog();
                scratchView4.clear();
                scratchView4.setVisibility(View.INVISIBLE);
            }
        });

        scratchView5.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent > 0) {
                    OnRevealScratchImage(5, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, scratchImage5, scratchImage6);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog();
                scratchView5.clear();
                scratchView5.setVisibility(View.INVISIBLE);
            }
        });

        scratchView6.setEraseStatusListener(new MegaGigaScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if (percent > 0) {
                    OnRevealScratchImage(6, scratchView1, scratchView2, scratchView3, scratchView4,
                            scratchView5, scratchView6, scratchImage1, scratchImage2, scratchImage3,
                            scratchImage4, scratchImage5, scratchImage6);
                }
            }

            @Override
            public void onCompleted(View view) {
                percentCounter++;
                showWinningDialog();
                scratchView6.clear();
                scratchView6.setVisibility(View.INVISIBLE);
            }
        });

        dialog.show();

        ShowScratchIntroductionDialog();

//                } else {
//                    Log.d("IsWinningCard", response.errorBody().toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<IsWinningCardDataPrime> call, Throwable t) {
//
//            }
//        });
    }


    private void ShowScratchIntroductionDialog(){
        SharedPreferences prefs = ctx.getSharedPreferences("user_info", MODE_PRIVATE);
        if(prefs.getBoolean("show_scratch_instructions", true)) {
            final Dialog dialog = new Dialog(RocketLevelActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_scratch_introduction);
            Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
            btnContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.hide();
                }
            });
            dialog.show();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("show_scratch_instructions", false);
            editor.commit();
        }
    }

    private void OnRevealScratchImage(int ScratchNo, MegaGigaScratchView scratchView1, MegaGigaScratchView scratchView2, MegaGigaScratchView scratchView3,
                                      MegaGigaScratchView scratchView4, MegaGigaScratchView scratchView5, MegaGigaScratchView scratchView6,
                                      ImageView imageView1, ImageView imageView2, ImageView imageView3, ImageView imageView4,
                                      ImageView imageView5, ImageView imageView6)
    {
        playSound();
        if (!open_fields.contains(ScratchNo)) {
            open_fields.add(ScratchNo);

            if (open_fields.size() == 2 && !isWinningTicket) {
                for (int i = 1; i < 7; i++) {
                    if (!open_fields.contains(i)) {
                        switch (i) {
                            case 1:
                                imageView1.setImageResource(images.get(lost_image));
                                break;
                            case 2:
                                imageView2.setImageResource(images.get(lost_image));
                                break;
                            case 3:
                                imageView3.setImageResource(images.get(lost_image));
                                break;
                            case 4:
                                imageView4.setImageResource(images.get(lost_image));
                                break;
                            case 5:
                                imageView5.setImageResource(images.get(lost_image));
                                break;
                            case 6:
                                imageView6.setImageResource(images.get(lost_image));
                                break;
                        }
                    }

                }
            }

            if(open_fields.size()==3){
                LockUnreaveledFields(scratchView1, scratchView2, scratchView3, scratchView4, scratchView5,
                        scratchView6, imageView1, imageView2, imageView3, imageView4, imageView5, imageView6);
            }
        }
    }

    private void LockUnreaveledFields(MegaGigaScratchView scratchView1, MegaGigaScratchView scratchView2, MegaGigaScratchView scratchView3,
                                      MegaGigaScratchView scratchView4, MegaGigaScratchView scratchView5, MegaGigaScratchView scratchView6,
                                      ImageView imageView1, ImageView imageView2, ImageView imageView3, ImageView imageView4,
                                      ImageView imageView5, ImageView imageView6){
        locked = true;
        over=true;
        if(!open_fields.contains(1)){
            imageView1.setImageResource(R.drawable.lock);
            scratchView1.setVisibility(View.INVISIBLE);
            scratchView1.clear();
        }
        if(!open_fields.contains(2)){
            imageView2.setImageResource(R.drawable.lock);
            scratchView2.setVisibility(View.INVISIBLE);
            scratchView2.clear();
        }
        if(!open_fields.contains(3)){
            imageView3.setImageResource(R.drawable.lock);
            scratchView3.setVisibility(View.INVISIBLE);
            scratchView3.clear();
        }
        if(!open_fields.contains(4)){
            imageView4.setImageResource(R.drawable.lock);
            scratchView4.setVisibility(View.INVISIBLE);
            scratchView4.clear();
        }
        if(!open_fields.contains(5)){
            imageView5.setImageResource(R.drawable.lock);
            scratchView5.setVisibility(View.INVISIBLE);
            scratchView5.clear();
        }
        if(!open_fields.contains(6)) {
            imageView6.setImageResource(R.drawable.lock);
            scratchView6.setVisibility(View.INVISIBLE);
            scratchView6.clear();
        }

    }

    private void showWinningDialog() {
        Log.e("Show winning dialog ", "percent counter : " + percentCounter);
        if(open_fields.size() == 6 && percentCounter == 6 && isWinningTicket ) {
            over=true;
            final Dialog dialog = new Dialog(RocketLevelActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_win);

            Button redeem = (Button) dialog.findViewById(R.id.redeem_button);
            Button later = (Button) dialog.findViewById(R.id.later_btn);

            redeem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(RocketLevelActivity.this, WalletActivity.class);
                    startActivity(intent);
                }
            });

            later.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            dialog.show();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void playSound() {
        if(!over) {
            // Is the sound loaded does it already play?
            synchronized (RocketLevelActivity.this) {
                if (loaded && !plays) {
                    soundPool.play(soundID, volume, volume, 1, 0, 1f);
                    counter = counter++;
                    //Toast.makeText(this, "Played sound", Toast.LENGTH_SHORT).show();
                    plays = true;
                }
            }
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    plays = false;
                    soundID = soundPool.load(ctx, R.raw.scratchnew, counter);
                }
            }, 1000);
        }
    }
}
