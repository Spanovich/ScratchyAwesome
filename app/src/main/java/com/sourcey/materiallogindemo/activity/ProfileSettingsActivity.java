package com.sourcey.materiallogindemo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.login.Logout;
import com.sourcey.materiallogindemo.api.login.entities.LoginData;
import com.sourcey.materiallogindemo.api.reset.ResetEmail;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lokal on 9/30/2017.
 */

public class ProfileSettingsActivity extends AppCompatActivity {

    private static final int ACTION_REQUEST_GALLERY = 99;
    private Button profilePic;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profilesettings);

        profilePic = (Button) findViewById(R.id.profile_btn);

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGallery();
            }
        });

        Button btnLogout = (Button)findViewById(R.id.btnLogout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(ProfileSettingsActivity.this,
                        R.style.Theme_AppCompat_DayNight_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Logging out...");
                progressDialog.show();
                Logout logout = new Logout(ProfileSettingsActivity.this);
                logout.logout().Logout().enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.errorBody() != null){
                            progressDialog.hide();
                            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else{
                            Toast.makeText(getBaseContext(), "An error occured in logout proccess!", Toast.LENGTH_LONG).show();
                            progressDialog.hide();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getBaseContext(), "An error occured in logout proccess!", Toast.LENGTH_LONG).show();
                        progressDialog.hide();
                    }
                });
            }
        });

        Button btnUpdate = (Button) findViewById(R.id.btnUpdate);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                final ProgressDialog progressDialog = new ProgressDialog(ProfileSettingsActivity.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Email reset...");
                progressDialog.show();
                final String new_email = txtEmail.getText().toString();
                if(!new_email.isEmpty()){
                    ResetEmail reset_email = new ResetEmail();
                    reset_email.resetEmaiRequest().ResetEmailRequest(new_email).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.errorBody() == null){
                                Toast.makeText(ProfileSettingsActivity.this, "We sent mail to new email address! Please input recieved code!",
                                        Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }
                            else{
                                Toast.makeText(ProfileSettingsActivity.this, "An error occured!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(ProfileSettingsActivity.this, "An error occured!", Toast.LENGTH_LONG).show();
                            progressDialog.hide();
                        }
                    });
                }
            }
        });
    }

    public void startGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        Intent chooser = Intent.createChooser(intent, "Choose a Picture");
        startActivityForResult(chooser, ACTION_REQUEST_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Detects request codes
        if(requestCode==ACTION_REQUEST_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                saveToPrefs(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void saveToPrefs(Bitmap image){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();

        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit=shre.edit();
        edit.putString("image_data",encodedImage);
        edit.putBoolean("set_profile_pic", true);
        edit.commit();
    }
}
