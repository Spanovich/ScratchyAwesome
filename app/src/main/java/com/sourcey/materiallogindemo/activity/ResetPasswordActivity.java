package com.sourcey.materiallogindemo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.api.reset.ResetPassword;

import butterknife.Bind;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by K-Android 001 on 10/13/2017.
 */

public class ResetPasswordActivity extends AppCompatActivity {
    String reset_token;
    String email;
    String password;
    String password_confim;
    EditText _emailText;
    EditText _passwordText;
    EditText _passwordConfirmText;
    Button _submitButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Intent intent = getIntent();
        Uri data = intent.getData();
        String link = data.toString();

        reset_token = link.substring(link.lastIndexOf('/') + 1);

        _emailText = (EditText) findViewById(R.id.input_email);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _passwordConfirmText = (EditText) findViewById(R.id.input_password_confirm);
        _submitButton = (Button) findViewById(R.id.btnSubmit);

        _submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                email = _emailText.getText().toString();
                password = _passwordText.getText().toString();
                password_confim = _passwordConfirmText.getText().toString();

                if (isDataValid()) {

                    final ProgressDialog progressDialog = new ProgressDialog(ResetPasswordActivity.this,
                            R.style.Theme_AppCompat_DayNight_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Sending reset password link...");
                    progressDialog.show();

                    ResetPassword resetPassword = new ResetPassword(ResetPasswordActivity.this);
                    resetPassword.resetPasswordConfirmation().resetPasswordConfirmation(reset_token, email, password, password_confim).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.errorBody() == null) {
                                Toast.makeText(getBaseContext(), "Reset password successfully!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                                finish();
                            } else {
                                Toast.makeText(getBaseContext(), "An error occured!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getBaseContext(), "An error occured!", Toast.LENGTH_LONG).show();
                            progressDialog.hide();
                        }
                    });
                }
                else{
                    Toast.makeText(getBaseContext(), "Reset password failed!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean isDataValid(){
        boolean valid=true;

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6) {
            _passwordText.setError("6 or more alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (password_confim.isEmpty() || password_confim.length() < 6 || !(password_confim.equals(password))) {
            _passwordConfirmText.setError("Password does not match");
            valid = false;
        } else {
            _passwordConfirmText.setError(null);
        }

        return valid;
    }
}