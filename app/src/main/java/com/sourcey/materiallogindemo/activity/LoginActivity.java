package com.sourcey.materiallogindemo.activity;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.adapter.DailyRewardAdapter;
import com.sourcey.materiallogindemo.api.gameplay.UserInfo;
import com.sourcey.materiallogindemo.api.gameplay.entities.UserInfoDataPrime;
import com.sourcey.materiallogindemo.api.login.Login;
import com.sourcey.materiallogindemo.api.login.entities.LoginData;
import com.sourcey.materiallogindemo.api.reset.ResetPassword;
import com.sourcey.materiallogindemo.api.reset.entities.ResetPasswordData;
import com.sourcey.materiallogindemo.api.rewards.DailyRewards;
import com.sourcey.materiallogindemo.api.rewards.entities.DailyRewardsData;
import com.sourcey.materiallogindemo.api.rewards.entities.DailyRewardsDataPrime;
import com.sourcey.materiallogindemo.api.rewards.entities.Rewards;
import com.sourcey.materiallogindemo.service.SendRefreshTokenService;
import com.sourcey.materiallogindemo.service.SoundService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import butterknife.ButterKnife;
import butterknife.Bind;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @Bind(R.id.input_email) EditText _emailText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.btn_login) Button _loginButton;
    @Bind(R.id.btn_signup) Button _signupButton;
//    @Bind(R.id.link_signup) TextView _signupLink;
    @Bind(R.id.link_pass_forget) TextView _passForgetLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _passForgetLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                        R.style.Theme_AppCompat_DayNight_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Sending reset password link...");
                progressDialog.show();

                String email = _emailText.getText().toString();

                ResetPassword resetPassword = new ResetPassword(LoginActivity.this);

                resetPassword.resetPasswordRequest().resetPasswordRequest(email).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.body()!=null){
                            Toast.makeText(getBaseContext(), "We sent you reset link!", Toast.LENGTH_LONG).show();
                            progressDialog.hide();
                        }
                        else{
                            Toast.makeText(getBaseContext(), "An error occured!", Toast.LENGTH_LONG).show();
                            progressDialog.hide();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getBaseContext(), "An error occured!", Toast.LENGTH_LONG).show();
                        progressDialog.hide();
                    }
                });
            }
        });

//        _signupLink.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // Start the Signup activity
//                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
//                startActivityForResult(intent, REQUEST_SIGNUP);
//                finish();
//                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//            }
//        });

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        loginApi(email, password, progressDialog, LoginActivity.this);

//        new android.os.Handler().postDelayed(
//                new Runnable() {
//                    public void run() {
//                        // On complete call either onLoginSuccess or onLoginFailed
//                        onLoginSuccess();
//                        // onLoginFailed();
//                        progressDialog.dismiss();
//                    }
//                }, 3000);
    }


    public void loginApi(String email, String password, final ProgressDialog progressDialog, final Context ctx){


        Login login = new Login(ctx);

        login.login().getUser(email, password).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.body()!=null){
                    LoginData data = new LoginData();
                    SharedPreferences preferences = ctx.getSharedPreferences("user_info", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("isUserLoggedIn", true);
                    editor.putString("access_token",response.body().getAccessToken());
                    editor.putString("refresh_token",response.body().getRefreshToken());
                    editor.commit();
                    Log.d("PostSuccess", "Access token :  " + response.body().getAccessToken());
//                    getDailyRewards(ctx);
                    progressDialog.hide();
                    onLoginSuccess();
                }
                else{
                    onLoginFailed();
                    progressDialog.hide();
                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                Log.d("PostFailure", t.getMessage());
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the GameplayActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
//        _loginButton.setEnabled(true);
//        Toast.makeText(getBaseContext(), "Login successful!", Toast.LENGTH_LONG).show();
        setRefreshTokenService(getApplicationContext());
        finish();
    }

    public void setRefreshTokenService(Context context) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);

        Intent intent = new Intent(context, SendRefreshTokenService.class);

        PendingIntent pintent = PendingIntent.getService(context, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                55*60*1000, pintent);

        startService(new Intent(getBaseContext(), SendRefreshTokenService.class));
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private void getDailyRewards(final Context ctx) {
            DailyRewards dailyRewards = new DailyRewards(ctx);
            dailyRewards.dailyRewards().getDailyReward().enqueue(new Callback<DailyRewardsDataPrime>() {
                @Override
                public void onResponse(Call<DailyRewardsDataPrime> call, Response<DailyRewardsDataPrime> response) {
                    if (response.errorBody() == null) {
                        GetUserInfoData(ctx);
//                        if (!response.body().data.seen) {
                            ShowDailyRewardDialog(response.body().getData(), ctx);
//                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<DailyRewardsDataPrime> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "An error occured!", Toast.LENGTH_LONG);
                }
            });
    }

    private void ShowDailyRewardDialog(DailyRewardsData dailyRewardsData, Context ctx){
        final Dialog dailyRewardDialog = new Dialog(GameplayActivity.ctx);
        dailyRewardDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dailyRewardDialog.setContentView(R.layout.dialog_daily_rewards);
        ListView listView = (ListView) dailyRewardDialog.findViewById(R.id.listviewRewards);
        ArrayList<Rewards> rewards = new ArrayList<Rewards>();
        rewards = dailyRewardsData.rewards;
        Collections.reverse(rewards);
        listView.setAdapter(new DailyRewardAdapter(ctx, R.layout.list_view_daily_rewards_item, rewards, dailyRewardsData.current_reward));
        Button btnTake = (Button) dailyRewardDialog.findViewById(R.id.btnTakeReward);
        Button btnX = (Button) dailyRewardDialog.findViewById(R.id.btnX);
        btnTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dailyRewardDialog.hide();
            }
        });
        btnX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dailyRewardDialog.hide();
            }
        });
        dailyRewardDialog.show();
    }

    private void GetUserInfoData(final Context ctx){

        UserInfo userInfo = new UserInfo(ctx);

        userInfo.getUserInfo().getUserInfo().enqueue(new Callback<UserInfoDataPrime>() {

            @Override
            public void onResponse(Call<UserInfoDataPrime> call, Response<UserInfoDataPrime> response) {
                if(response.body()!=null){
                        Log.d("UserInfoDetails", response.body().data.toString());
                    SharedPreferences settings = ctx.getSharedPreferences("user_info",
                            Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("first_name", response.body().getData().getFirstName());
                    editor.putString("last_name", response.body().getData().getLastName());
                    editor.putString("gender", response.body().getData().getGender());
                    editor.putString("email", response.body().getData().getEmail());
                    editor.putInt("coins", response.body().getData().getPersonal().getCoins());
                    editor.putInt("available_cards", response.body().getData().getPersonal().getAvailableCards());
                    editor.putInt("notifications", response.body().getData().getPersonal().getNotifications());
                    editor.commit();

                }
                else{
                    try {
                        Log.d("UserInfoDetails", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserInfoDataPrime> call, Throwable t) {

            }
        });
    }
}
