package com.sourcey.materiallogindemo.utils;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;

/**
 * Created by lokal on 9/5/2017.
 */

public class Utils {

    private Context ctx;
    private DisplayMetrics displaymetrics;
    private float width;
    private float height;

    public Utils(Context context){
        ctx = context;
        displaymetrics = ctx.getResources().getDisplayMetrics();
        if (displaymetrics.widthPixels < displaymetrics.heightPixels) {
            width = displaymetrics.heightPixels;
            height = displaymetrics.widthPixels;
        } else {
            width = displaymetrics.widthPixels;
            height = displaymetrics.heightPixels;
        }
    }

    public float getWidth(){
        return width;
    }

    public float getHeight(){
        return height;
    }

    public String getManufactruer(){
        return Build.MANUFACTURER;
    }

    public String getModel(){
        return Build.MODEL;
    }

    public String getAndroidVersion(){
        return Build.VERSION.RELEASE;
    }






}
