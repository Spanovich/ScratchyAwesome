package com.sourcey.materiallogindemo.model;

import java.util.Date;

/**
 * Created by lokal on 8/20/2017.
 */

public class CrimeChild {



    private String year;
    private Date mDate;
    private String code;


    public CrimeChild(String year, String code) {
        this.year = year;
        this.code = code;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
