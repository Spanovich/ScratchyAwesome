package com.sourcey.materiallogindemo.model;

/**
 * Created by lokal on 10/29/2017.
 */

public class Pixel {

    public Pixel(float x, float y){
        this.x = x;
        this.y = y;
    }

    private float x,y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
