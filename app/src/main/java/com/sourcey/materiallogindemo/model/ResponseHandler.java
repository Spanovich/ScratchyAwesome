package com.sourcey.materiallogindemo.model;

import java.util.ArrayList;

/**
 * Created by lokal on 10/29/2017.
 */

public class ResponseHandler {

    private ArrayList<Pixel> pixels;

    public ArrayList<Pixel> getPoints() {
        return pixels;
    }

    public void setPoints(ArrayList<Pixel> points) {
        this.pixels = points;
    }
}
