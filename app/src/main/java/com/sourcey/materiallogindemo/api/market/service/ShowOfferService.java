package com.sourcey.materiallogindemo.api.market.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface ShowOfferService {
    @GET("v1/market/offers/{offer}")
    Call<ResponseBody> showOffer(@Path("offer") String offer);
}
