package com.sourcey.materiallogindemo.api.reset.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 10/2/2017.
 */

public interface ResetEmailConformationService {
    @POST("v1/profile/email/{code}")
    Call<ResponseBody> ResetEmailConfirmation(@Path("code") String code);
}
