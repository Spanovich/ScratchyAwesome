package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 12/2/2017.
 */

public class MapPlacementsData {
    public String id;
    public int position;
    public String type;
    public GiftboxData giftbox;
    public RocketLevelData rocket_level;

    public String getId(){return id;}
    public void setId(String id){this.id = id;}

    public int getPosition(){return position;}
    public void setPosition(int position){this.position = position;}

    public String getType(){return type;}
    public void setType(String type){this.type = type;}

    public GiftboxData getGiftbox(){return giftbox;}
    public void setGiftbox(GiftboxData giftbox){this.giftbox = giftbox;}

    public RocketLevelData getRocketLevel(){return rocket_level;}
    public void setRocketLevel(RocketLevelData rocket_level){this.rocket_level = rocket_level;}
}
