package com.sourcey.materiallogindemo.api.facebook.entities;

/**
 * Created by K-Android 001 on 9/30/2017.
 */

public class FacebookLoginData {
    public String access_token;
    public String refresh_token;
    public int expires_in;

    public String getAccessToken() {
        return access_token;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public String getRefreshToken() {
        return refresh_token;
    }

    public void setRefreshToken(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public int getExpiration() {
        return expires_in;
    }

    public void setExpiration(int expires_in) {
        this.expires_in = expires_in;
    }
}
