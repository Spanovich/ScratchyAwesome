package com.sourcey.materiallogindemo.api.gameplay.entities;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 12/2/2017.
 */

public class MapPlacements {
    public ArrayList<MapPlacementsData> placements;

    public ArrayList<MapPlacementsData> getPlacements(){return placements;}
    public void setPlacements(ArrayList<MapPlacementsData> placements){this.placements = placements;}
}
