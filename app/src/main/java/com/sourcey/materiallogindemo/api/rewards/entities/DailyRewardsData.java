package com.sourcey.materiallogindemo.api.rewards.entities;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 10/23/2017.
 */

public class DailyRewardsData {
    public ArrayList<Rewards> rewards;
    public Rewards current_reward;
    public boolean seen;


    public ArrayList<Rewards> getRewards() {
        return rewards;
    }
    public void setRewards(ArrayList<Rewards> rewards) {
        this.rewards = rewards;
    }

    public Rewards getCurrentReward() {
        return current_reward;
    }
    public void setCurrentReward(Rewards current_reward) {
        this.current_reward = current_reward;
    }

    public boolean getSeen() {
        return seen;
    }
    public void setSeen(boolean seen) {
        this.seen = seen;
    }
}
