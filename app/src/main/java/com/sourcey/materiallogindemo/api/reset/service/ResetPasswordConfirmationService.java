package com.sourcey.materiallogindemo.api.reset.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 10/13/2017.
 */

public interface ResetPasswordConfirmationService {
    @FormUrlEncoded
    @POST("/v1/auth/password/confirm/{token}")
    Call<ResponseBody> resetPasswordConfirmation( @Path("token") String token1, @Field("email") String email1, @Field("password") String password1,
                                                  @Field("password_confirmation") String password_confirmation1);
}
