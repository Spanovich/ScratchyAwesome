package com.sourcey.materiallogindemo.api.rewards.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface RewardsService {
    @GET("v1/rewards")
    Call<ResponseBody> getRewards();
}
