package com.sourcey.materiallogindemo.api.rewards.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface RedeemService {
    @POST("v1/rewards/{reward}/address")
    Call<ResponseBody> redeemReward(@Path("reward") String reward, @Field("first_name") String first_name, @Field("last_name") String last_name,
                                    @Field("phone") String phone, @Field("address") String address, @Field("zip") String zip,
                                    @Field("city") String city, @Field("country") String country);
}
