package com.sourcey.materiallogindemo.api.market.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface BuyOfferService {
    @FormUrlEncoded
    @POST("v1/market/buy/{offer}")
    Call<ResponseBody> buyOffer(@Path("offer") String offer, @Field("data") String data);
}
