package com.sourcey.materiallogindemo.api.rewards.entities;

/**
 * Created by K-Android 001 on 10/23/2017.
 */

public class Rewards {
    public String id;
    public String type;
    public int value;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}
