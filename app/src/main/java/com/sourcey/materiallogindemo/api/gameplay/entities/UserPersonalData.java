package com.sourcey.materiallogindemo.api.gameplay.entities;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 11/9/2017.
 */

public class UserPersonalData {
    public RocketLevelData rocket_level;
    public ArrayList<GiftboxesData> giftboxes;
    public ArrayList<ChestsData> chests;
    public int days_active;
    public int coins;
    public int available_cards;
    public int notifications;

    public RocketLevelData getRocketLevel(){ return rocket_level; }
    public void setRocketLevel(RocketLevelData rocket_level) { this.rocket_level = rocket_level; }

    public ArrayList<GiftboxesData> getGiftboxes(){ return giftboxes; }
    public void setGiftboxes(ArrayList<GiftboxesData> giftboxes) { this.giftboxes = giftboxes; }

    public ArrayList<ChestsData> getChests(){ return chests; }
    public void setChests(ArrayList<ChestsData> chests) { this.chests = chests; }

    public int getDaysActive(){ return days_active; }
    public void setDaysActive(int days_active) { this.days_active = days_active; }

    public int getCoins(){ return coins; }
    public void setCoins(int coins) { this.coins = coins; }

    public int getAvailableCards(){ return available_cards; }
    public void setAvailableCards(int available_cards) { this.available_cards = available_cards; }

    public int getNotifications(){ return notifications; }
    public void setNotifications(int notifications) { this.notifications = notifications; }
}
