package com.sourcey.materiallogindemo.api.register.entities;

/**
 * Created by K-Android 001 on 9/25/2017.
 */

public class RegisterData {
    public String id;
    public String first_name;
    public String last_name;
    public String email;
    public String birthday;
    public String image;
    public String state;
    public String zip;
    public String city;
    public String street;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return first_name;
    }
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return last_name;
    }
    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.last_name = birthday;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.image = zip;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.zip = state;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
}
