package com.sourcey.materiallogindemo.api.login;

import com.sourcey.materiallogindemo.api.API;
import com.sourcey.materiallogindemo.api.login.service.RefreshLoginService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public class RefreshLogin {

    private OkHttpClient okHttpClient;
    private Retrofit retrofit;

    protected Retrofit.Builder retrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient());
    }

    protected synchronized OkHttpClient okHttpClient() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            okHttpClient = builder.build();
        }
        return okHttpClient;
    }



    protected Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = retrofitBuilder().build();
        }
        return retrofit;
    }

    public RefreshLoginService refreshLoginService() {
        return getRetrofit().create(RefreshLoginService.class);
    }
}
