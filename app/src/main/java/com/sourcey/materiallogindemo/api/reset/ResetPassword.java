package com.sourcey.materiallogindemo.api.reset;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sourcey.materiallogindemo.api.API;
import com.sourcey.materiallogindemo.api.reset.service.ResetPasswordConfirmationService;
import com.sourcey.materiallogindemo.api.reset.service.ResetPasswordService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by K-Android 001 on 10/2/2017.
 */

public class ResetPassword {
    SharedPreferences prefs;

    public ResetPassword(Context ctx){
        prefs =  PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    private OkHttpClient okHttpClient;
    private Retrofit retrofit;

    protected Retrofit.Builder retrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient());
    }

    protected synchronized OkHttpClient okHttpClient() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    String token = prefs.getString("device_token", "");

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("X-Device-Token", token);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            okHttpClient = builder.build();
        }
        return okHttpClient;
    }



    protected Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = retrofitBuilder().build();
        }
        return retrofit;
    }

    public ResetPasswordService resetPasswordRequest() {
        return getRetrofit().create(ResetPasswordService.class);
    }

    public ResetPasswordConfirmationService resetPasswordConfirmation() {
        return getRetrofit().create(ResetPasswordConfirmationService.class);
    }
}
