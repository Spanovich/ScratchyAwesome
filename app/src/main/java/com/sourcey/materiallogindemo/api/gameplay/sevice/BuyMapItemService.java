package com.sourcey.materiallogindemo.api.gameplay.sevice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface BuyMapItemService {
    @POST("v1/map/{placement}/buy")
    Call<ResponseBody> buyMapItem(@Path("placement") String placement);
}
