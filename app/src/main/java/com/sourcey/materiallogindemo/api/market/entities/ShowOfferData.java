package com.sourcey.materiallogindemo.api.market.entities;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public class ShowOfferData {
    public String id;
    public String name;
    public String type;
    public String description;
    public String image;
    public String identifier;
    public int price;
    public int price_coins;
    public int position;
    public ArrayList<String> items;

    public String getId(){return id;};
    public void setId(String id){this.id = id;}

    public String getName(){return name;};
    public void setName(String name){this.name = name;}

    public String getType(){return type;};
    public void setType(String type){this.type = type;}

    public String getDescription(){return description;};
    public void setDescription(String description){this.description = description;}

    public String getImage(){return image;};
    public void setImage(String image){this.image = image;}

    public String getIdentifier(){return identifier;};
    public void setIdentifier(String identifier){this.identifier = identifier;}

    public int getPrice(){return price;};
    public void setPrice(int price){this.price = price;}

    public int getPrice_coins(){return price_coins;};
    public void setPrice_coins(int price_coins){this.price_coins = price_coins;}

    public int getPosition(){return position;};
    public void setPosition(int position){this.position = position;}

    public ArrayList<String> getItems(){return items;};
    public void setItems( ArrayList<String> items){this.items = items;}
}
