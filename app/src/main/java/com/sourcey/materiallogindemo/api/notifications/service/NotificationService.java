package com.sourcey.materiallogindemo.api.notifications.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by lokal on 10/11/2017.
 */

public interface NotificationService {

    @FormUrlEncoded
    @POST("/v1/android/message")
    Call<ResponseBody> pushToken(@Field("token") String token);
}
