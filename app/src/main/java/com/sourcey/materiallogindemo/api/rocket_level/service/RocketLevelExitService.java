package com.sourcey.materiallogindemo.api.rocket_level.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface RocketLevelExitService {
    @DELETE("v1/rocket-levels")
    Call<ResponseBody> exitRocketLevel();
}
