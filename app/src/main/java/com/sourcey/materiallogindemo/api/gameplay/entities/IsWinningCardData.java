package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 11/9/2017.
 */

public class IsWinningCardData {
    public String id;
    public CardData card;
    public String deck;
    public boolean win;
    public String status;

    public String getId(){return id;}
    public void setId(String id){this.id=id;}

    public CardData getCard(){return card;}
    public void setCard(CardData card){this.card=card;}

    public String getDeck(){return deck;}
    public void setDeck(String deck){this.deck=deck;}

    public boolean getWin(){return win;}
    public void setWin(boolean win){this.win=win;}

    public String getStatus(){return status;}
    public void setStatus(String status){this.status=status;}
}
