package com.sourcey.materiallogindemo.api.reset.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 10/2/2017.
 */

public interface ResetEmaiRequestService {
    @FormUrlEncoded
    @POST("v1/profile/email")
    Call<ResponseBody> ResetEmailRequest(@Field("email") String email);
}
