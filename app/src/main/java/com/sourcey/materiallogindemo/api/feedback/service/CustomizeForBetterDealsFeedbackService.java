package com.sourcey.materiallogindemo.api.feedback.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface CustomizeForBetterDealsFeedbackService {
    @POST("v1/questions/{question}/answers/{answer}")
    Call<ResponseBody> sendFeedback(@Path("question") String question, @Path("answer") String answer);

}
