package com.sourcey.materiallogindemo.api.feedback.entities;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class CustomizeForBetterDealsData {
    public String id;
    public String content;
    public String image;
    public String type;
    public int order;
    public ArrayList<CustomizeForBetterDealsAnswers> answers;

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getContent() { return content; }
    public void setContent(String content) { this.content = content; }

    public String getImage() { return image; }
    public void setImage(String image) { this.image = image; }

    public String getType() { return type; }
    public void setType(String type) { this.type = type; }

    public int getOrder() { return order; }
    public void setOrder(int order) { this.order = order; }

    public ArrayList<CustomizeForBetterDealsAnswers> getAnswers() { return answers; }
    public void setAnswers(ArrayList<CustomizeForBetterDealsAnswers> answers) { this.answers = answers; }
}
