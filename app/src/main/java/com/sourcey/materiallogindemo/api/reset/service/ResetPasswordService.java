package com.sourcey.materiallogindemo.api.reset.service;

import com.sourcey.materiallogindemo.api.reset.entities.ResetPasswordData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 10/2/2017.
 */

public interface ResetPasswordService {
    @FormUrlEncoded
    @POST("v1/auth/password/email")
    Call<ResponseBody> resetPasswordRequest(@Field("email") String email1);
}
