package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class RocketLevelData {
    public String id;
    public String name;
    public String description;
    public int price;
    public int levels;
    public String starts_at;
    public String ends_at;

    public String getId(){return id;}
    public void setId(String id){this.id = id;}

    public String getName(){return name;}
    public void setName(String name){this.name = name;}

    public String getDescription(){return description;}
    public void setDescription(String description){this.description = description;}

    public int getPrice(){return price;}
    public void setPrice(int price){this.price = price;}

    public int getLevels(){return levels;}
    public void setLevels(int levels){this.levels = levels;}

    public String getStartsAt(){return starts_at;}
    public void setStartsAt(String starts_at){this.starts_at = starts_at;}

    public String getEndsAt(){return ends_at;}
    public void setEndsAt(String ends_at){this.ends_at = ends_at;}
}
