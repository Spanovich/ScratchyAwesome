package com.sourcey.materiallogindemo.api.facebook.services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface FacebookDisconnectService {
    @POST("v1/social/facebook/disconnect")
    Call<ResponseBody> facebookDisconnect();

}
