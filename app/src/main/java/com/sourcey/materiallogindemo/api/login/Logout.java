package com.sourcey.materiallogindemo.api.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.sourcey.materiallogindemo.api.API;
import com.sourcey.materiallogindemo.api.login.service.LogoutService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by K-Android 001 on 10/1/2017.
 */

public class Logout {
//    SharedPreferences prefs;
    Context ctx;

    public Logout(Context ctx){
//        prefs =  PreferenceManager.getDefaultSharedPreferences(ctx);
        this.ctx = ctx;
    }

    private OkHttpClient okHttpClient;
    private Retrofit retrofit;

    protected Retrofit.Builder retrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient());
    }

    protected synchronized OkHttpClient okHttpClient() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    SharedPreferences prefs = ctx.getSharedPreferences("user_info", MODE_PRIVATE);
                    String token = prefs.getString("access_token", "");

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Authorization", "Bearer " + token)
                            .addHeader("User-Agent", "Android");

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            okHttpClient = builder.build();
        }
        return okHttpClient;
    }



    protected Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = retrofitBuilder().build();
        }
        return retrofit;
    }

    public LogoutService logout() {
        return getRetrofit().create(LogoutService.class);
    }
}
