package com.sourcey.materiallogindemo.api.login.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 10/1/2017.
 */

public interface LogoutService {
    @POST("v1/auth/logout")
    Call<ResponseBody> Logout();
}
