package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class GiftboxesData {
    public String id;
    public int position;
    public int used_cards;
    public GiftboxData giftboxData;

    public String getId(){return id;}
    public void setId(String id){this.id = id;}

    public int getPosition(){return position;}
    public void setPosition(int position){this.position = position;}

    public int getUsedCards(){return used_cards;}
    public void setUsedCards(int used_cards){this.used_cards = used_cards;}

    public GiftboxData getGiftboxData(){return giftboxData;}
    public void setGiftboxData(GiftboxData giftboxData){this.giftboxData = giftboxData;}
}
