package com.sourcey.materiallogindemo.api.gameplay.sevice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 12/2/2017.
 */

public interface ShowCardService {
    @GET("v1/cards/{card}")
    Call<ResponseBody> showCard(@Path("card") String card);
}
