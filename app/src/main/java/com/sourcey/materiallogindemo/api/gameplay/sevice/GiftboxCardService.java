package com.sourcey.materiallogindemo.api.gameplay.sevice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by K-Android 001 on 12/2/2017.
 */

public interface GiftboxCardService {
    @GET("v1/giftboxes/{giftbox}/pull")
    Call<ResponseBody> getGiftboxCard(@Path("giftbox") String giftbox);
}
