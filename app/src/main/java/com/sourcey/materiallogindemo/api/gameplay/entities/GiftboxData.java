package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class GiftboxData {
    public String id;
    public int price;
    public int total_cards;

    public String getId(){return id;}
    public void setId(String id){this.id = id;}

    public int getPrice(){return price;}
    public void setPrice(int price){this.price = price;}

    public int getTotalCards(){return total_cards;}
    public void setTotalCards(int total_cards){this.total_cards = total_cards;}
}
