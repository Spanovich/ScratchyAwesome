package com.sourcey.materiallogindemo.api.reset;

import android.content.SharedPreferences;

import com.sourcey.materiallogindemo.api.API;
import com.sourcey.materiallogindemo.api.reset.service.ResetEmaiRequestService;
import com.sourcey.materiallogindemo.api.reset.service.ResetEmailConformationService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by K-Android 001 on 10/2/2017.
 */

public class ResetEmail {
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;

    protected Retrofit.Builder retrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient());
    }

    protected synchronized OkHttpClient okHttpClient() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    SharedPreferences prefs = getApplicationContext().getSharedPreferences("user_info", MODE_PRIVATE);
                    String token = prefs.getString("access_token", "");

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Authorization", "Bearer " + token);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            okHttpClient = builder.build();
        }
        return okHttpClient;
    }



    protected Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = retrofitBuilder().build();
        }
        return retrofit;
    }

    public ResetEmailConformationService resetEmailConformation() {
        return getRetrofit().create(ResetEmailConformationService.class);
    }

    public ResetEmaiRequestService resetEmaiRequest() {
        return getRetrofit().create(ResetEmaiRequestService.class);
    }
}
