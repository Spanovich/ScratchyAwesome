package com.sourcey.materiallogindemo.api.notifications.entities;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public class NotificationsToggleData {
    public boolean data;

    public boolean getData(){return data;}
    public void setData(boolean data){this.data = data;}
}
