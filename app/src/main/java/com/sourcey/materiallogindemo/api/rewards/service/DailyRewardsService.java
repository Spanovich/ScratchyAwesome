package com.sourcey.materiallogindemo.api.rewards.service;
import com.sourcey.materiallogindemo.api.rewards.entities.DailyRewardsData;
import com.sourcey.materiallogindemo.api.rewards.entities.DailyRewardsDataPrime;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by K-Android 001 on 10/23/2017.
 */

public interface DailyRewardsService {
    @GET("v1/profile/daily-reward")
    Call<DailyRewardsDataPrime> getDailyReward();
}
