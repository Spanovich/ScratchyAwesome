package com.sourcey.materiallogindemo.api.gameplay.sevice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface CardScratchedService {
    @POST("v1/cards/scratch")
    Call<ResponseBody> postScratchedCard();
}
