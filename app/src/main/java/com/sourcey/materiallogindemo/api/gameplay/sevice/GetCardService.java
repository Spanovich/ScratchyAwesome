package com.sourcey.materiallogindemo.api.gameplay.sevice;

import com.sourcey.materiallogindemo.api.gameplay.entities.IsWinningCardDataPrime;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by K-Android 001 on 11/9/2017.
 */

public interface GetCardService {
    @GET("v1/cards/pull")
    Call<IsWinningCardDataPrime> getIsWinningCard();
}
