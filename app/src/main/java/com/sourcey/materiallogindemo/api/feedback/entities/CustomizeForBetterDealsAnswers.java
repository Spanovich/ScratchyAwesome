package com.sourcey.materiallogindemo.api.feedback.entities;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class CustomizeForBetterDealsAnswers {
    public String id;
    public String content;
    public int order;

    public String getId(){return id;}
    public void setId(String id){this.id = id;}

    public String getContent(){return content;}
    public void setContent(String content){this.content = content;}

    public int getOrder(){return order;}
    public void setOrder(int order){this.order = order;}


}
