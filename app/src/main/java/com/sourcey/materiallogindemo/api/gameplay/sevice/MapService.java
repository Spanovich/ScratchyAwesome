package com.sourcey.materiallogindemo.api.gameplay.sevice;

import com.sourcey.materiallogindemo.api.gameplay.entities.MapData;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface MapService {
    @GET("v1/map")
    Call<MapData> getMap();
}
