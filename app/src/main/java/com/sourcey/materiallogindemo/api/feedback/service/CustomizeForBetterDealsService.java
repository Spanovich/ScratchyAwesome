package com.sourcey.materiallogindemo.api.feedback.service;

import com.sourcey.materiallogindemo.api.feedback.entities.CustomizeForBetterDealsDataPrime;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public interface CustomizeForBetterDealsService {
    @GET("v1/questions")
    Call<CustomizeForBetterDealsDataPrime> getQuestionsForCustomization();
}
