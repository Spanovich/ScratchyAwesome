package com.sourcey.materiallogindemo.api.login.service;

import com.sourcey.materiallogindemo.api.login.entities.LoginData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface RefreshLoginService {
    @FormUrlEncoded
    @POST("v1/auth/login/refresh")
    Call<LoginData> sendRefreshAuth(@Field("refresh_token") String refresh_token);
}
