package com.sourcey.materiallogindemo.api.facebook.services;

import com.sourcey.materiallogindemo.api.login.entities.LoginData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 9/27/2017.
 */

public interface FacebookLoginService {
    @FormUrlEncoded
    @POST("v1/auth/login/facebook")
    Call<LoginData> getUser(@Field("token") String token);
}
