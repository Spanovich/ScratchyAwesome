package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 11/9/2017.
 */

public class UserInfoData {

    public String id;
    public String first_name;
    public String last_name;
    public String gender;
    public String email;
    public String birthday;
    public String image;
    public String state;
    public String zip;
    public String city;
    public String street;
    public int position;
    public UserPersonalData personal;

    public String getId(){ return id; }
    public void setId(String id){ this.id = id; }

    public String getFirstName(){ return id; }
    public void setFirstName(String first_name){ this.first_name = first_name; }

    public String getLastName(){ return last_name; }
    public void setLastName(String last_name){ this.last_name = last_name; }

    public String getGender(){ return gender; }
    public void setGender(String gender){ this.gender = gender; }

    public String getEmail(){ return email; }
    public void setEmail(String email){ this.email = email; }

    public String getBirthday(){ return birthday; }
    public void setBirthday(String birthday){ this.birthday = birthday; }

    public String getImage(){ return image; }
    public void setImage(String image){ this.image = image; }

    public String getState(){ return state; }
    public void setState(String state){ this.state = state; }

    public String getZip(){ return zip; }
    public void setZip(String zip){ this.zip = zip; }

    public String getCity(){ return city; }
    public void setCity(String city){ this.city = city; }

    public String getStreet(){ return street; }
    public void setStreet(String street){ this.street = street; }

    public int getPosition(){ return position; }
    public void setPosition(int position){ this.position = position; }

    public UserPersonalData getPersonal(){ return personal; }
    public void setPersonal(UserPersonalData personal){ this.personal = personal; }
}
