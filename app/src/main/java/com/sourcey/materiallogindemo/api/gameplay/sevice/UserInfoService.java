package com.sourcey.materiallogindemo.api.gameplay.sevice;

import com.sourcey.materiallogindemo.api.gameplay.entities.UserInfoDataPrime;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 11/9/2017.
 */

public interface UserInfoService {
    @POST("v1/profile")
    Call<UserInfoDataPrime> getUserInfo();
}
