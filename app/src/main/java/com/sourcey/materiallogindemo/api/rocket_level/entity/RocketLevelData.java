package com.sourcey.materiallogindemo.api.rocket_level.entity;

/**
 * Created by K-Android 001 on 10/29/2017.
 */

public class RocketLevelData {

    public boolean isWinning;

    public RocketLevelData(boolean isWinning){
        this.isWinning = isWinning;
    }

    public boolean getIsWinning(){ return isWinning; };
    public void setIsWinning(boolean isWinning){ this.isWinning = isWinning; };
}
