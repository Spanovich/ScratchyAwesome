package com.sourcey.materiallogindemo.api.gameplay.sevice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface DiscardMapItemService {
    @POST("v1/map/discard")
    Call<ResponseBody> discardMapItem();
}
