package com.sourcey.materiallogindemo.api.register.service;

import com.sourcey.materiallogindemo.api.register.entities.RegisterData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 9/25/2017.
 */

public interface RegisterService {
    @FormUrlEncoded
    @POST("v1/auth/register")
    Call<RegisterData> SignUpUser(@Field("first_name") String first_name, @Field("last_name") String last_name, @Field("email") String email,
                                  @Field("password") String password, @Field("password_confirmation") String password_confirmation,
                                  @Field("gender") String gender);
}
