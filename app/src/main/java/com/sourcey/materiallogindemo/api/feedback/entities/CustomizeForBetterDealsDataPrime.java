package com.sourcey.materiallogindemo.api.feedback.entities;

import java.util.ArrayList;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class CustomizeForBetterDealsDataPrime {
    public ArrayList<CustomizeForBetterDealsData> data;

    public ArrayList<CustomizeForBetterDealsData> getData(){ return data; }
    public void setData(ArrayList<CustomizeForBetterDealsData> data){ this.data = data; }
}
