package com.sourcey.materiallogindemo.api.notifications.service;

import com.sourcey.materiallogindemo.api.notifications.entities.NotificationsToggleData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by K-Android 001 on 11/25/2017.
 */

public interface NotificationToggleService {
    @POST("/v1/profile/notifications")
    Call<NotificationsToggleData> toggleNotifications();
}
