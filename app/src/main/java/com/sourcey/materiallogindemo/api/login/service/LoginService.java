package com.sourcey.materiallogindemo.api.login.service;

/**
 * Created by lokal on 8/19/2017.
 */

import com.sourcey.materiallogindemo.api.login.entities.LoginData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LoginService {
    @FormUrlEncoded
    @POST("v1/auth/login")
    Call<LoginData> getUser(@Field("email") String email1, @Field("password") String password1);
}
