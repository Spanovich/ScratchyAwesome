package com.sourcey.materiallogindemo.api.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sourcey.materiallogindemo.api.API;
import com.sourcey.materiallogindemo.api.login.service.LoginService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lokal on 8/19/2017.
 */

public class Login {

    SharedPreferences prefs;

    public Login(Context ctx){
        prefs =  PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    private OkHttpClient okHttpClient;
    private Retrofit retrofit;

    protected Retrofit.Builder retrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient());
    }

    protected synchronized OkHttpClient okHttpClient() {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    String token = prefs.getString("device_token", "");

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("X-Device-Token", token);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            okHttpClient = builder.build();
        }
        return okHttpClient;
    }



    protected Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = retrofitBuilder().build();
        }
        return retrofit;
    }

    public LoginService login() {
        return getRetrofit().create(LoginService.class);
    }
}
