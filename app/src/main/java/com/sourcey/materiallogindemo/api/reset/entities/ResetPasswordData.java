package com.sourcey.materiallogindemo.api.reset.entities;

/**
 * Created by K-Android 001 on 10/2/2017.
 */

public class ResetPasswordData {
    public boolean data;
    public String status;

    public boolean getData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
