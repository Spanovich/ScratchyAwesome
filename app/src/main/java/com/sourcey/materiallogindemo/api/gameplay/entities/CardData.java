package com.sourcey.materiallogindemo.api.gameplay.entities;

/**
 * Created by K-Android 001 on 11/16/2017.
 */

public class CardData {
    public String id;
    public String name;
    public String type;
    public String thumb_image;
    public String prize_image;
    public String prize_description;
    public String prize_data;

    public String getId(){return id;}
    public void setId(String id){this.id=id;}

    public String getName(){return name;}
    public void setName(String name){this.name=name;}

    public String getType(){return type;}
    public void setType(String type){this.type=type;}

    public String getThumbImage(){return thumb_image;}
    public void setThumbImage(String thumb_image){this.thumb_image=thumb_image;}

    public String getPrizeImage(){return prize_image;}
    public void setPrizeImage(String prize_image){this.prize_image=prize_image;}

    public String getPrizeDescription(){return prize_description;}
    public void setPrizeDescription(String prize_description){this.prize_description=prize_description;}

    public String getPrizeData(){return prize_data;}
    public void setPrizeData(String prize_data){this.prize_data=prize_data;}
}
