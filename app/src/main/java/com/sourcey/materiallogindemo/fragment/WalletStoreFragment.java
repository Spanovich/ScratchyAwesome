package com.sourcey.materiallogindemo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Bojan Cvetojevic on 12/5/2017.
 */

public class WalletStoreFragment extends Fragment {

    @Bind(R.id.llPromotionsHolder)
    LinearLayout llPromotionsHolder;

    @Bind(R.id.llCardsHeader)
    LinearLayout llCardsHeader;

    @Bind(R.id.llChestsHeader)
    LinearLayout llChestsHeader;

    @Bind(R.id.llRocketsHeader)
    LinearLayout llRocketsHeader;

    @Bind(R.id.llCoinsHeader)
    LinearLayout llCoinsHeader;

    @Bind(R.id.llCoinOptions)
    LinearLayout llCoinOptions;

    @Bind(R.id.llChestOptions)
    LinearLayout llChestOptions;

    @Bind(R.id.llRocketOptions)
    LinearLayout llRocketOptions;

    @Bind(R.id.llCardOptions)
    LinearLayout llCardOptions;

    private LayoutInflater inflater;

    public static WalletStoreFragment newInstance() {

        Bundle args = new Bundle();

        WalletStoreFragment fragment = new WalletStoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wallet_store, container, false);
        ButterKnife.bind(this, rootView);

        this.inflater = inflater;

        initPromotions();
        initCoinOptions();
        initChestOptions();
        initRocketOptions();
        initCardOptions();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    //----------------------------------------------------------------------------------------------
    // Purchase Click handlers
    //----------------------------------------------------------------------------------------------

    private View.OnClickListener promotionOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String sku = (String) v.getTag();
            Toast.makeText(getActivity(), "promotion sku " + sku, Toast.LENGTH_SHORT).show();
            // TODO: 12/11/2017 Purchase or whatever
        }
    };

    private View.OnClickListener coinOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String sku = (String) v.getTag();
            Toast.makeText(getActivity(), "coin sku " + sku, Toast.LENGTH_SHORT).show();
            // TODO: 12/11/2017 Purchase or whatever
        }
    };

    private View.OnClickListener chestOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String sku = (String) v.getTag();
            Toast.makeText(getActivity(), "chest sku " + sku, Toast.LENGTH_SHORT).show();
            // TODO: 12/11/2017 Purchase or whatever
        }
    };

    private View.OnClickListener cardOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String sku = (String) v.getTag();
            Toast.makeText(getActivity(), "card sku " + sku, Toast.LENGTH_SHORT).show();
            // TODO: 12/11/2017 Purchase or whatever
        }
    };

    //----------------------------------------------------------------------------------------------
    // Toggle Inner Purchase Options
    //----------------------------------------------------------------------------------------------

    @OnClick(R.id.llCardsHeader)
    public void onCardOptionClick() {
        if (!llCardOptions.isActivated()) {
            llCardOptions.setActivated(true);
            llCardOptions.setVisibility(View.VISIBLE);
        }

        llCoinOptions.setActivated(false);
        llChestOptions.setActivated(false);
        llCoinOptions.setVisibility(View.GONE);
        llChestOptions.setVisibility(View.GONE);
    }

    @OnClick(R.id.llCoinsHeader)
    public void onCoinOptionClick() {
        if (!llCoinOptions.isActivated()) {
            llCoinOptions.setActivated(true);
            llCoinOptions.setVisibility(View.VISIBLE);
        }

        llCardOptions.setActivated(false);
        llChestOptions.setActivated(false);
        llCardOptions.setVisibility(View.GONE);
        llChestOptions.setVisibility(View.GONE);
    }

    @OnClick(R.id.llChestsHeader)
    public void onChestOptionClick() {
        if (!llChestOptions.isActivated()) {
            llChestOptions.setActivated(true);
            llChestOptions.setVisibility(View.VISIBLE);
        }

        llCoinOptions.setActivated(false);
        llCardOptions.setActivated(false);
        llCoinOptions.setVisibility(View.GONE);
        llCardOptions.setVisibility(View.GONE);
    }

    //----------------------------------------------------------------------------------------------
    // View Inflation / Init / Utility
    //----------------------------------------------------------------------------------------------


    private void initCoinOptions() {
        // Init header
        setHeaderData(R.drawable.ic_coins_header, "Coins", "starting from\n$0.99", llCoinsHeader);

        // Init options
        View fiveHundredCoins
                = inflateWalletOptionItem(R.drawable.ic_500_coins, "500 coins", "$0.99", getString(R.string.sku_inapp_2_cards), coinOnClickListener);
        View oneThousandCoins
                = inflateWalletOptionItem(R.drawable.ic_1000_coins, "1000 coins", "$1.49", getString(R.string.sku_inapp_4_cards), coinOnClickListener);
        View twoThousandCoins
                = inflateWalletOptionItem(R.drawable.ic_2000_coins, "2000 coins", "$2.49", getString(R.string.sku_inapp_6_cards), coinOnClickListener);

        llCoinOptions.addView(fiveHundredCoins);
        llCoinOptions.addView(oneThousandCoins);
        llCoinOptions.addView(twoThousandCoins);

        llCoinOptions.setVisibility(View.GONE);
    }


    private void initCardOptions() {
        // Init header
        setHeaderData(R.drawable.ic_cards_header, "Cards", "starting from\n$0.99", llCardsHeader);

        // Init options
        View twoCardsOption 
                = inflateWalletOptionItem(R.drawable.ic_2_cards, "Two cards", "$0.99", getString(R.string.sku_inapp_2_cards), cardOnClickListener);
        View fourCardsOption 
                = inflateWalletOptionItem(R.drawable.ic_4_cards, "Four cards", "$1.49", getString(R.string.sku_inapp_4_cards), cardOnClickListener);
        View sixCardsOption 
                = inflateWalletOptionItem(R.drawable.ic_6_cards, "Siz cards", "$2.49", getString(R.string.sku_inapp_6_cards), cardOnClickListener);

        llCardOptions.addView(twoCardsOption);
        llCardOptions.addView(fourCardsOption);
        llCardOptions.addView(sixCardsOption);

        llCardOptions.setVisibility(View.GONE);
    }

    private void initRocketOptions() {
        setHeaderData(R.drawable.ic_rockets_header, "Rocket keys", "starting from\n$0.99", llRocketsHeader);
        // TODO: 12/11/2017 There are no inner fields on the design?
    }

    private void initChestOptions() {
        // Init header
        setHeaderData(R.drawable.ic_chests_header, "Chests", "starting from\n$0.99", llChestsHeader);

        // Init options
        View boosterChestOption
                = inflateWalletOptionItem(R.drawable.ic_booster_chest, "Booster chest", "$0.99", getString(R.string.sku_inapp_booster_chest), chestOnClickListener);
        View bronzeChestOption
                = inflateWalletOptionItem(R.drawable.ic_bronze_chest, "Bronze chest", "$2.49", getString(R.string.sku_inapp_bronze_chest), chestOnClickListener);
        View silverChestOption
                = inflateWalletOptionItem(R.drawable.ic_silver_chest, "Silver Chest", "$3.49", getString(R.string.sku_inapp_silver_chest), chestOnClickListener);
        View goldChestOption
                = inflateWalletOptionItem(R.drawable.ic_gold_chest, "Gold Chest", "$4.49", getString(R.string.sku_inapp_gold_chest), chestOnClickListener);

        llChestOptions.addView(boosterChestOption);
        llChestOptions.addView(bronzeChestOption);
        llChestOptions.addView(silverChestOption);
        llChestOptions.addView(goldChestOption);

        llChestOptions.setVisibility(View.GONE);
    }

    private void initPromotions() {
        View offerItem1 = inflater.inflate(R.layout.wallet_store_promotion_item, llPromotionsHolder, false);
        View offerItem2 = inflater.inflate(R.layout.wallet_store_promotion_item, llPromotionsHolder, false);
        View offerItem3 = inflater.inflate(R.layout.wallet_store_promotion_item, llPromotionsHolder, false);

        llPromotionsHolder.addView(offerItem1);
        llPromotionsHolder.addView(offerItem2);
        llPromotionsHolder.addView(offerItem3);

        setPromotionItemData("https://thumbs.dreamstime.com/b/rocket-drawing-space-red-planets-54301263.jpg",
                "All Roket Boster 1", "10% off", "Buy for $1.5", "promotionSku", offerItem1);
        setPromotionItemData("https://thumbs.dreamstime.com/z/rocket-vector-drawing-ufo-jet-booster-velocity-rocketship-vessel-start-takeoff-send-to-orbit-mission-freehand-outline-ink-drawn-66713798.jpg",
                "All Roket Boster 2", "20% off", "Buy for $2.5", "promotionSku", offerItem2);
        setPromotionItemData("https://i2.wp.com/mindbenders.eu/wp-content/uploads/2017/05/JCua6sK9M5U-1.jpg",
                "All Roket Boster 3", "30% off", "Buy for $3.5", "promotionSku", offerItem3);

    }

    /**
     * @param iconResId
     * @param desc
     * @param price
     * @param sku             Product Sku will be used as the Layout tag
     * @param onClickListener Optional onClick listener that will be placed on the returned view
     * @return
     */
    private View inflateWalletOptionItem(int iconResId, String desc, String price, String sku, View.OnClickListener onClickListener) {
        View optionItem = inflater.inflate(R.layout.wallet_store_option_item, null);

        setOptionItemData(iconResId, desc, price, optionItem);
        optionItem.setTag(sku);

        optionItem.setOnClickListener(onClickListener);

        return optionItem;
    }

    /**
     * Sets the option item icon, desc and price. This is used for both the header and option items. (keep that in mind if you change some layout ids)
     *
     * @param iconResId
     * @param desc
     * @param price
     * @param headerView
     */
    private void setOptionItemData(int iconResId, String desc, String price, View headerView) {
        ImageView optionIcon = (ImageView) headerView.findViewById(R.id.ivOptionIcon);
        TextView tvOptionDesc = (TextView) headerView.findViewById(R.id.tvOptionDesc);
        TextView tvOptionPrice = (TextView) headerView.findViewById(R.id.tvOptionPrice);

        optionIcon.setImageResource(iconResId);
        tvOptionDesc.setText(desc);
        tvOptionPrice.setText(price);
    }

    /**
     * Sets the option item icon, desc and price. This is used for both the header and option items. (keep that in mind if you change some layout ids)
     * Loads the icon from the given url
     *
     * @param iconUrl
     * @param desc
     * @param price
     * @param headerView
     */
    private void setOptionItemData(String iconUrl, String desc, String price, View headerView) {
        ImageView optionIcon = (ImageView) headerView.findViewById(R.id.ivOptionIcon);
        TextView tvOptionDesc = (TextView) headerView.findViewById(R.id.tvOptionDesc);
        TextView tvOptionPrice = (TextView) headerView.findViewById(R.id.tvOptionPrice);

        Picasso.with(getActivity())
                .load(iconUrl)
                .fit()
                .centerInside()
                .into(optionIcon);

        tvOptionDesc.setText(desc);
        tvOptionPrice.setText(price);
    }

    private void setHeaderData(int iconResId, String desc, String price, View headerView) {
        setOptionItemData(iconResId, desc, price, headerView);
    }

    /**
     * Loads the offer promotion item data and sets the {@link #promotionOnClickListener} on click listener
     *
     * @param bgImageUrl
     * @param offerName
     * @param offerDisc
     * @param offerPrice
     * @param offerSku
     * @param offerView
     */
    private void setPromotionItemData(String bgImageUrl, String offerName, String offerDisc, String offerPrice, String offerSku, View offerView) {
        ImageView ivOfferBg = (ImageView) offerView.findViewById(R.id.ivOfferBackground);
        TextView tvOfferName = (TextView) offerView.findViewById(R.id.tvOfferName);
        TextView tvOfferDiscount = (TextView) offerView.findViewById(R.id.tvOfferDiscount);
        TextView tvOfferBuy = (TextView) offerView.findViewById(R.id.tvOfferBuy);

        // TODO: 12/11/2017 Ask vladeta for an offer placeholder while the actual image is loaded
        Picasso.with(getActivity())
                .load(bgImageUrl)
                .fit()
                .centerCrop()
                .into(ivOfferBg);

        tvOfferName.setText(offerName);
        tvOfferDiscount.setText(offerDisc);
        tvOfferBuy.setText(offerPrice);

        offerView.setTag(offerSku);
        offerView.setOnClickListener(promotionOnClickListener);
    }

}
