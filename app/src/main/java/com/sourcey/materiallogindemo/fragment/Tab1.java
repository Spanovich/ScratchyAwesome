package com.sourcey.materiallogindemo.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.activity.ProfileSettingsActivity;
import com.sourcey.materiallogindemo.adapter.GridViewAdapter;
import com.sourcey.materiallogindemo.api.feedback.CustomizeForBetterDeals;
import com.sourcey.materiallogindemo.api.feedback.entities.CustomizeForBetterDealsAnswers;
import com.sourcey.materiallogindemo.api.feedback.entities.CustomizeForBetterDealsData;
import com.sourcey.materiallogindemo.api.feedback.entities.CustomizeForBetterDealsDataPrime;
import com.sourcey.materiallogindemo.layout.ExpandableHeightGridView;
import com.sourcey.materiallogindemo.model.ImageItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lokal on 8/19/2017.
 */

public class Tab1 extends Fragment {

    private ExpandableHeightGridView gridView;
    private GridViewAdapter gridAdapter;
    private Button settings;
    private Button customize;
    private Button facebookLogin;
    private Button inviteSms;
    private TextView settingsText, getCoins;
    private ArrayList<CustomizeForBetterDealsData> questions;
    private int question_counter;

    private CircleImageView profilePicture;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tab_1, container, false);

        gridView = (ExpandableHeightGridView) v.findViewById(R.id.imagesGrid);
        gridAdapter = new GridViewAdapter(getActivity(), R.layout.grid_item_layout, getData());
        gridView.setAdapter(gridAdapter);
        gridView.setExpanded(true);

        getCoins = (TextView) v.findViewById(R.id.get_coins_text);

        settings = (Button) v.findViewById(R.id.settings_icon);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ProfileSettingsActivity.class);
                startActivity(i);
            }
        });

        settingsText = (TextView) v.findViewById(R.id.settings);
        settingsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ProfileSettingsActivity.class);
                startActivity(i);
            }
        });

        facebookLogin = (Button) v.findViewById(R.id.facebook_login_btn);
        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appLinkUrl = "http://scratchy-game.devpresta.co";

                if (AppInviteDialog.canShow()) {
                    AppInviteContent content = new AppInviteContent.Builder()
                            .setApplinkUrl(appLinkUrl)
                            .build();
                    AppInviteDialog.show(getActivity(), content);
                }
//                gridView.setVisibility(View.VISIBLE);
//                facebookLogin.setVisibility(View.GONE);
//                getCoins.setVisibility(View.GONE);
//                AccessToken token = AccessToken.getCurrentAccessToken();
//                GraphRequest graphRequest = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
//                        try {
//                            String user_id = jsonObject.getString("id");
//                            myNewGraphReq(user_id);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                Bundle param = new Bundle();
//                param.putString("fields", "id");
//                graphRequest.setParameters(param);
//                graphRequest.executeAsync();
            }
        });

        profilePicture = (CircleImageView) v.findViewById(R.id.profile_image);

        inviteSms = (Button) v.findViewById(R.id.invite_btn);
        inviteSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "12346556";  // The number on which you want to send SMS
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
            }
        });

        customize = (Button) v.findViewById(R.id.customize_btn);

        customize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                CustomizeForBetterDeals customizeForBetterDeals = new CustomizeForBetterDeals(getActivity());
                customizeForBetterDeals.customizeForBetterDealsService().getQuestionsForCustomization().enqueue(new Callback<CustomizeForBetterDealsDataPrime>() {
                    @Override
                    public void onResponse(Call<CustomizeForBetterDealsDataPrime> call, Response<CustomizeForBetterDealsDataPrime> response) {
                        if (response.body() != null) {
                            questions = response.body().data;

                            Collections.sort(questions, new Comparator<CustomizeForBetterDealsData>() {
                                public int compare(CustomizeForBetterDealsData o1, CustomizeForBetterDealsData o2) {
                                    return o1.order - o2.order;
                                }
                            });

                            question_counter = 0;

                            getCustomizeQuestion(question_counter);
                        }
                        else{
                            try {
                                Log.d("CustomizeForBetterDeals", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomizeForBetterDealsDataPrime> call, Throwable t) {

                    }
                });
            }
        });

        return v;
    }

    private void myNewGraphReq(String friendlistId) {
        final String graphPath = "/"+friendlistId+"/friends/";
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest request = new GraphRequest(token, graphPath, null, HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                JSONObject object = graphResponse.getJSONObject();
                try {
                    JSONArray arrayOfUsersInFriendList= object.getJSONArray("data");
                /* Do something with the user list */
                /* ex: get first user in list, "name" */
                    JSONObject user = arrayOfUsersInFriendList.getJSONObject(0);
                    String usersName = user.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "name");
        request.setParameters(param);
        request.executeAsync();
    }

    private void getCustomizeQuestion(int question_index){
        if (questions.size() > question_index && question_index >= 0) {
            CustomizeForBetterDealsData question = questions.get(question_index);
            switch (question.type) {
                case "radio":
                    Collections.sort(question.answers, new Comparator<CustomizeForBetterDealsAnswers>() {
                        public int compare(CustomizeForBetterDealsAnswers a1, CustomizeForBetterDealsAnswers a2) {
                            return a1.order - a2.order;
                        }
                    });
                    showDialogRadio(question.getContent(), question.answers);
                    break;
                case "scale":
                    showDialogScale(question.getContent());
                    break;
            }
        }
        else if (question_index<0){
            question_counter = questions.size() - 1;
            getCustomizeQuestion(question_counter);
        }
        else{
            question_counter = 0;
            getCustomizeQuestion(question_counter);
        }
    }

    public void showDialogScale(String quesstion_text){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_question);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(quesstion_text);
                /*ImageView image = (ImageView) dialog.findViewById(R.id.image);
                image.setImageResource(R.drawable.ic_launcher);*/

        RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rating);
        ratingBar.setNumStars(5);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if ( fromUser ) {
                    ratingBar.setRating(rating );
                }
            }
        });

        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button previousQuestion = (Button) dialog.findViewById(R.id.previousQuestion);
        previousQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_counter--;
                getCustomizeQuestion(question_counter);
                dialog.dismiss();
            }
        });

        Button nextQuestion = (Button) dialog.findViewById(R.id.nextQuestion);

        nextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_counter++;
                getCustomizeQuestion(question_counter);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDialogRadio(String question_text, ArrayList<CustomizeForBetterDealsAnswers> ansewers){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_radiobutton_question);

        TextView text_question = (TextView) dialog.findViewById(R.id.text_question);
        text_question.setText(question_text);
        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioLikes);


        // find the radiobutton by returned id

        RadioButton radio1 = (RadioButton) dialog.findViewById(R.id.radio1);
        RadioButton radio2 = (RadioButton) dialog.findViewById(R.id.radio2);
        RadioButton radio3 = (RadioButton) dialog.findViewById(R.id.radio3);

        radio1.setText(ansewers.get(0).getContent());
        radio2.setText(ansewers.get(1).getContent());
        radio3.setText(ansewers.get(2).getContent());

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton radioSexButton = (RadioButton) dialog.findViewById(checkedId);
                Toast.makeText(getActivity(), "Ivan likes " + radioSexButton.getText(), Toast.LENGTH_SHORT ).show();
            }
        });

        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        // if button is clicked, close the custom dialog
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        Button previousQuestion = (Button) dialog.findViewById(R.id.previousQuestion);
        previousQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_counter--;
                getCustomizeQuestion(question_counter);
                dialog.dismiss();
            }
        });

        Button nextQuestion = (Button) dialog.findViewById(R.id.nextQuestion);

        nextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_counter++;
                getCustomizeQuestion(question_counter);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    // Prepare some dummy data for gridview
    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
            imageItems.add(new ImageItem(bitmap, "Image#" + i));
        }
        return imageItems;
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(getActivity());

            String previouslyEncodedImage = shre.getString("image_data", "");

            if( !previouslyEncodedImage.equalsIgnoreCase("") ){
                byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                profilePicture.setImageBitmap(bitmap);
            }

    }
}
