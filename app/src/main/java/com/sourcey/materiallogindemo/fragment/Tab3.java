package com.sourcey.materiallogindemo.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sourcey.materiallogindemo.R;

/**
 * Created by lokal on 8/19/2017.
 */

public class Tab3 extends Fragment {
    private boolean isMoreCardsOpen=false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tab_3, container, false);

        final Button btnMoreCards = (Button) v.findViewById(R.id.btnBuyMoreCards);
        final LinearLayout firstRowLayout = (LinearLayout) v.findViewById(R.id.firstRowLayout);
        final LinearLayout secondRowLayout = (LinearLayout) v.findViewById(R.id.secondRowLayout);
        final LinearLayout moreCardsLayout = (LinearLayout) v.findViewById(R.id.moreCardsLayout);
        final LinearLayout coinsSelectedLayout = (LinearLayout) v.findViewById(R.id.coinsSelectedLayout);
        final LinearLayout chestsSelectedLayout = (LinearLayout) v.findViewById(R.id.chestsSelectedLayout);
        final ImageView imgCoins = (ImageView) v.findViewById(R.id.imgCoins);
        final ImageView imgRocketLevel = (ImageView) v.findViewById(R.id.imgRocketLevel);
        final ImageView imgChests = (ImageView) v.findViewById(R.id.imgChests);
        final View border1 = (View) v.findViewById(R.id.border1);
        final View border2 = (View) v.findViewById(R.id.border2);
        final View border3 = (View) v.findViewById(R.id.border3);

        btnMoreCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isMoreCardsOpen) {
                    btnMoreCards.setTextColor(Color.rgb(230, 190, 110));
                    btnMoreCards.setBackgroundResource(R.drawable.facebook_border);
                    moreCardsLayout.setVisibility(View.VISIBLE);
                    isMoreCardsOpen = true;
                }
                else{
                    btnMoreCards.setTextColor(Color.WHITE);
                    btnMoreCards.setBackgroundColor(Color.rgb(115,198,204));
                    moreCardsLayout.setVisibility(View.GONE);
                    isMoreCardsOpen = false;
                }

            }
        });

        imgCoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstRowLayout.setVisibility(View.GONE);
                secondRowLayout.setVisibility(View.GONE);
                moreCardsLayout.setVisibility(View.GONE);
                btnMoreCards.setVisibility(View.GONE);
                chestsSelectedLayout.setVisibility(View.GONE);
                border2.setVisibility(View.GONE);
                border3.setVisibility(View.GONE);
                coinsSelectedLayout.setVisibility(View.VISIBLE);
                imgCoins.setImageResource(R.drawable.coins_store_sel);
                imgChests.setImageResource(R.drawable.chests_store);
                imgRocketLevel.setImageResource(R.drawable.rocket_levels_store);
            }
        });

        imgChests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstRowLayout.setVisibility(View.GONE);
                secondRowLayout.setVisibility(View.GONE);
                moreCardsLayout.setVisibility(View.GONE);
                btnMoreCards.setVisibility(View.GONE);
                border2.setVisibility(View.GONE);
                border3.setVisibility(View.GONE);
                chestsSelectedLayout.setVisibility(View.VISIBLE);
                coinsSelectedLayout.setVisibility(View.GONE);
                imgCoins.setImageResource(R.drawable.coins_store);
                imgChests.setImageResource(R.drawable.chests_store_sel);
                imgRocketLevel.setImageResource(R.drawable.rocket_levels_store);
            }
        });

        return v;
    }

}
