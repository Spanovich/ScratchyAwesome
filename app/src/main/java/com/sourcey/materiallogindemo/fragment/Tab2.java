package com.sourcey.materiallogindemo.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.adapter.Tab2Adapter;
import com.sourcey.materiallogindemo.layout.SimpleDividerItemDecoration;
import com.sourcey.materiallogindemo.model.Crime;
import com.sourcey.materiallogindemo.model.CrimeChild;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by lokal on 8/19/2017.
 */

public class Tab2 extends Fragment {

    private RecyclerView recyclerView;
    private RelativeLayout topLevelLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tab_2, container, false);

        Tab2Adapter mCrimeExpandableAdapter = new Tab2Adapter(getActivity(), generateCrimes());
        mCrimeExpandableAdapter.setCustomParentAnimationViewId(R.id.details);
        mCrimeExpandableAdapter.setParentClickableViewAnimationDefaultDuration();
        mCrimeExpandableAdapter.setParentAndIconExpandOnClick(true);

        topLevelLayout =(RelativeLayout) v.findViewById(R.id.top_layout);
        topLevelLayout.setVisibility(View.GONE);

        final Button profileBtn = (Button) topLevelLayout.findViewById(R.id.instruction_btn);
        final Button winningBtn = (Button) topLevelLayout.findViewById(R.id.winning_section_btn);
        final Button skipBtn = (Button) topLevelLayout.findViewById(R.id.skip_btn);

        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileBtn.setVisibility(View.GONE);
                winningBtn.setVisibility(View.VISIBLE);
            }
        });

        winningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topLevelLayout.setVisibility(View.INVISIBLE);
            }
        });

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topLevelLayout.setVisibility(View.INVISIBLE);
                skipBtn.setVisibility(View.GONE);
            }
        });

        isFirstTime();

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(mCrimeExpandableAdapter);

        return v;
    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = getActivity().getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
            topLevelLayout.setVisibility(View.VISIBLE);
           /* topLevelLayout.setOnTouchListener(new View.OnTouchListener(){

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    topLevelLayout.setVisibility(View.INVISIBLE);
                    return false;
                }

            });*/


        }
        return ranBefore;

    }

    private ArrayList<ParentObject> generateCrimes() {

        List<Crime> crimes = new ArrayList<Crime>();
        Crime crime1 = new Crime();
        crime1.setTitle("12% off");
        Crime crime2 = new Crime();
        crime2.setTitle("13% off");
        crimes.add(crime1);
        crimes.add(crime2);
        ArrayList<ParentObject> parentObjects = new ArrayList<>();
        for (Crime crime : crimes) {
            ArrayList<Object> childList = new ArrayList<>();
            childList.add(new CrimeChild("DD/MM", "QwRY72"));
            crime.setChildObjectList(childList);
            parentObjects.add(crime);
        }
        return parentObjects;
    }
}
